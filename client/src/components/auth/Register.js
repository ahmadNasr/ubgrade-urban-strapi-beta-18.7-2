import React, { Component } from 'react';
import { withTranslation } from 'react-i18next';
import { Button, ModalBody } from 'mdbreact';
import { CountryDropdown } from 'react-country-region-selector';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { registerUser } from '../../actions/authActions';
import { connectModal } from 'redux-modal';

class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      username: '',
      email: '',
      operationsDesc: '',
      position: '',
      organization: '',
      country: '',
      password: '',
      passwordConf: '',
      success: false,
      loading: false,
      errors: {}
    };
  }

  componentDidMount() {
    document
      .getElementById('custom-select')
      .style.setProperty('color', 'rgba(158, 158, 158, .6)', 'important');
  }

  componentDidUpdate(prevProps) {
    if (this.props !== prevProps) {
      if (this.props.errors.register === null) {
        this.setState({ errors: {}, loading: false, success: true });
      }

      if (this.props.errors.register && this.props.errors.register.message) {
        this.setState({
          errors: this.props.errors.register.message,
          success: false,
          loading: false
        });
      }
    }
  }

  selectCountry(val) {
    const countrySelect = document.getElementById('custom-select');

    val === ''
      ? countrySelect.style.setProperty(
          'color',
          'rgba(158, 158, 158, .6)',
          'important'
        )
      : countrySelect.style.setProperty(
          'color',
          'rgba(73, 80, 87, 0.65)',
          'important'
        );

    this.setState({
      country: val,
      success: false,
      errors: { ...this.state.errors, country: '' }
    });
  }

  onChange = e => {
    this.setState({
      ...this.state,
      [e.target.name]: e.target.value,
      success: false,
      errors: { ...this.state.errors, [e.target.name]: '' }
    });
  };

  onSubmit = e => {
    e.preventDefault();

    this.setState({ loading: true, success: false });
    const newUser = {
      name: this.state.name,
      username: this.state.username,
      email: this.state.email.toLowerCase(),
      operationsDesc: this.state.operationsDesc,
      position: this.state.position,
      organization: this.state.organization,
      country: this.state.country,
      password: this.state.password,
      passwordConf: this.state.passwordConf
    };
    this.props.registerUser(newUser);
  };

  render() {
    const { errors, success, loading } = this.state;
    const { t } = this.props;

    return (
      <ModalBody className='register mx-3 p-0 px-2 pt-2 pb-3'>
        {errors.allow_register && (
          <div className='alert alert-danger text-center font-smaller-1'>
            {t(`forms.register-disallowed`)}
          </div>
        )}
        {success && (
          <div className='alert alert-success text-center font-smaller-1'>
            {t('forms.register-success')}
          </div>
        )}
        <form className='grey-text' noValidate onSubmit={this.onSubmit}>
          <div className='d-flex flex-row'>
            <div
              className={`d-flex flex-column w-50 ${!t('language.isRTL')
                ? 'mr-2'
                : 'ml-2'}`}>
              <label className='grey-text font-smaller-1 mb-1'>
                {t('forms.name')}
              </label>
              <input
                type='text'
                className={`form-control form-control-sm ${errors.name &&
                  'is-invalid'}`}
                name='name'
                value={this.state.name}
                onChange={this.onChange}
              />
              {errors.name && (
                <div className='invalid-feedback'>
                  {t(`forms.${errors.name}`)}
                </div>
              )}
            </div>
            <div
              className={`d-flex flex-column w-50 ${!t('language.isRTL')
                ? 'ml-2'
                : 'mr-2'}`}>
              <label className='grey-text font-smaller-1 mb-1'>
                {t('forms.username')}
              </label>
              <input
                type='text'
                className={`form-control form-control-sm ${errors.username &&
                  'is-invalid'}`}
                name='username'
                value={this.state.username}
                onChange={this.onChange}
              />
              {errors.username && (
                <div className='invalid-feedback'>
                  {t(`forms.${errors.username}`)}
                </div>
              )}
            </div>
          </div>
          <hr className='pb-3 m-0 bt-0' />
          <label className='grey-text font-smaller-1 mb-1'>
            {t('forms.email')}
          </label>
          <input
            type='email'
            placeholder={t('forms.work-email')}
            className={`form-control form-control-sm text-lowercase ${errors.email &&
              'is-invalid'}`}
            name='email'
            value={this.state.email.toLowerCase()}
            onChange={this.onChange}
          />
          {errors.email && (
            <div className='invalid-feedback'>{t(`forms.${errors.email}`)}</div>
          )}
          <hr className='pb-3 m-0 bt-0' />
          <label className='grey-text font-smaller-1 mb-1'>
            {t('forms.aoo')}
          </label>
          <input
            type='text'
            placeholder={t('forms.aoo-placeholder')}
            className={`form-control form-control-sm ${errors.operationsDesc &&
              'is-invalid'}`}
            name='operationsDesc'
            value={this.state.operationsDesc}
            onChange={this.onChange}
          />
          {errors.operationsDesc && (
            <div className='invalid-feedback'>
              {t(`forms.${errors.operationsDesc}`)}
            </div>
          )}
          <hr className='pb-3 m-0 bt-0' />
          <div className='d-flex flex-row'>
            <div
              className={`d-flex flex-column w-50 ${!t('language.isRTL')
                ? 'mr-2'
                : 'ml-2'}`}>
              <label className='grey-text font-smaller-1 mb-1'>
                {t('forms.pos')}
              </label>
              <input
                type='text'
                className={`form-control form-control-sm ${errors.position &&
                  'is-invalid'}`}
                name='position'
                value={this.state.position}
                onChange={this.onChange}
              />
              {errors.position && (
                <div className='invalid-feedback'>
                  {t(`forms.${errors.position}`)}
                </div>
              )}
            </div>
            <div
              className={`d-flex flex-column w-50 ${!t('language.isRTL')
                ? 'ml-2'
                : 'mr-2'}`}>
              <label className='grey-text font-smaller-1 mb-1'>
                {t('forms.org')}
              </label>
              <input
                type='text'
                className={`form-control form-control-sm ${errors.organization &&
                  'is-invalid'}`}
                name='organization'
                value={this.state.organization}
                onChange={this.onChange}
              />
              {errors.organization && (
                <div className='invalid-feedback'>
                  {t(`forms.${errors.organization}`)}
                </div>
              )}
            </div>
          </div>
          <hr className='pb-3 m-0 bt-0' />
          <label className='grey-text font-smaller-1 mb-1'>
            {t('forms.county')}
          </label>
          <div>
            <CountryDropdown
              defaultOptionLabel={t('forms.select-country')}
              id='custom-select'
              className={`form-control form-control-sm browser-default custom-select font-montserrat ${errors.country &&
                'is-invalid'}`}
              value={this.state.country}
              onChange={val => this.selectCountry(val)}
            />
          </div>
          {errors.country && (
            <div className='invalid-feedback'>
              {t(`forms.${errors.country}`)}
            </div>
          )}
          <hr className='pb-3 m-0 bt-0' />
          <div className='d-flex flex-row'>
            <div
              className={`d-flex flex-column w-50 ${!t('language.isRTL')
                ? 'mr-2'
                : 'ml-2'}`}>
              <label className='grey-text font-smaller-1 mb-1'>
                {t('forms.pwd')}
              </label>
              <input
                type='password'
                className={`form-control form-control-sm ${errors.password &&
                  'is-invalid'}`}
                name='password'
                value={this.state.password}
                onChange={this.onChange}
              />
              {errors.password && (
                <div className='invalid-feedback'>
                  {t(`forms.${errors.password}`)}
                </div>
              )}
            </div>
            <div
              className={`d-flex flex-column w-50 ${!t('language.isRTL')
                ? 'ml-2'
                : 'mr-2'}`}>
              <label className='grey-text font-smaller-1 mb-1'>
                {t('forms.conf-pwd')}
              </label>
              <input
                type='password'
                className={`form-control form-control-sm ${errors.passwordConf &&
                  'is-invalid'}`}
                name='passwordConf'
                value={this.state.passwordConf}
                onChange={this.onChange}
              />
              {errors.passwordConf && (
                <div className='invalid-feedback'>
                  {t(`forms.${errors.passwordConf}`)}
                </div>
              )}
            </div>
          </div>
          <Button
            type='submit'
            className={`font-weight-500 mt-4 mb-0 flex-center mx-auto ${loading &&
              'disabled'}`}
            color='info'
            size='sm'>
            {loading ? (
              <div
                className='spinner-grow text-white spinner-grow-sm'
                role='status'>
                <span className='sr-only'>{t('forms.loading')}</span>
              </div>
            ) : (
              <span>{t('forms.sign-up')}</span>
            )}
          </Button>
          {/*<div className="text-center mt-3 black-text">
             <hr className="hr-light" />
            <div className="d-flex text-center justify-content-center font-small blue-text mx-2 ">
              <p>
                By clicking <em>Sign up</em> you agree to our{' '}
                <a to="" target="_blank" className="blue-text">
                  <em className="blue-text">Terms of Service </em>
                </a>
                and{' '}
                <a to="" target="_blank">
                  <em className="blue-text">Data Use Policy</em>
                </a>
              </p>
            </div> 
          </div>*/}
        </form>
      </ModalBody>
    );
  }
}

Register.propTypes = {
  registerUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});

export default connectModal({ name: 'authModal' })(
  connect(mapStateToProps, { registerUser })(
    withRouter(withTranslation()(Register))
  )
);
