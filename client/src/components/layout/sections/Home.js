import React, { Component } from 'react';
import { HashRouter, Link } from 'react-router-dom';
import i18next from 'i18next';
import { withTranslation } from 'react-i18next';
import axios from 'axios';
import isEmpty from '../../../utils/is-empty';
import {
  Mask,
  View,
  Container,
  MDBRow,
  MDBCol,
  MDBBtn,
  MDBCard,
  MDBCardImage,
  MDBCardBody,
  MDBCardTitle,
  MDBCardFooter,
  MDBCarousel,
  MDBCarouselInner,
  MDBCarouselItem,
  toast,
  MDBContainer,
  MDBModal,
  MDBModalBody
} from 'mdbreact';
import ReactTooltip from 'react-tooltip';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { show } from 'redux-modal';
import mediumZoom from 'medium-zoom';
import { urbanUserGroups, urbanUserGroupsList } from '../../../json/data.js';
import toPersianDigits from '../../../utils/toPersianDigits';

const carouselSlidesPerRow = 3;
const damageAssessmentMapSeries =
  'https://urban-syria.org/proxy/esri/storymap-series/damage-assessment';

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalDamage: false,
      products: [],
      productsAr: []
    };
  }

  toggle = () => {
    this.setState({
      modalDamage: !this.state.modalDamage
    });
  };

  handleZoomImg = (pCode, productType) => () => {
    if (document.querySelector(`.${pCode}-${productType}`)) {
      const zoom = mediumZoom(
        document.querySelector(`.${pCode}-${productType}`),
        {
          background: 'rgba(1,1,1,0.5)',
          margin: 20
        }
      );
      zoom.open();
      zoom.on('closed', () => {
        zoom.detach();
      });
    }
  };

  componentDidMount() {
    setTimeout(() => {
      ReactTooltip.rebuild();
    }, 100);

    axios
      .all([
        axios.get(
          `${process.env.REACT_APP_API_URL}/products/recentCityFactSheet`,
          {
            headers: {
              ApiKey: process.env.REACT_APP_API_KEY
            }
          }
        ),
        axios.get(
          `${process.env.REACT_APP_API_URL}/products/recentCityFactSheetAr`,
          {
            headers: {
              ApiKey: process.env.REACT_APP_API_KEY
            }
          }
        ),
        localStorage.jwtToken
          ? axios.get(
              `${process.env
                .REACT_APP_API_URL}/products/recentCityProfileReport`,
              {
                headers: {
                  ApiKey: process.env.REACT_APP_API_KEY
                }
              }
            )
          : axios.get(
              `${process.env
                .REACT_APP_API_URL}/products/recentCityProfileReportSkeleton`,
              {
                headers: {
                  ApiKey: process.env.REACT_APP_API_KEY
                }
              }
            )
      ])
      .then(
        axios.spread(
          (
            recentCityFactSheet,
            recentCityFactSheetAr,
            recentCityProfileReport
          ) => {
            const recentCityFactSheetData = recentCityFactSheet.data;
            const recentCityProfileReportData = recentCityProfileReport.data;
            const mergedProducts = recentCityFactSheetData.concat(
              recentCityProfileReportData
            );
            mergedProducts.sort((a, b) => {
              if (a.updatedAt < b.updatedAt) {
                return 1;
              }
              if (a.updatedAt > b.updatedAt) {
                return -1;
              }
              return 0;
            });

            let recentCityFactSheetDataAr = recentCityFactSheetAr.data.concat(
              recentCityFactSheetData
            );
            recentCityFactSheetDataAr = recentCityFactSheetDataAr.filter(
              (product, index, self) =>
                self.findIndex(t => t.city.pCode === product.city.pCode) ===
                index
            );
            const mergedProductsAr = recentCityFactSheetDataAr.concat(
              recentCityProfileReportData
            );
            mergedProductsAr.sort((a, b) => {
              if (a.updatedAt < b.updatedAt) {
                return 1;
              }
              if (a.updatedAt > b.updatedAt) {
                return -1;
              }
              return 0;
            });

            this.setState({
              products: mergedProducts,
              productsAr: mergedProductsAr
            });
          }
        )
      )
      .catch(error => {
        console.log('Error loading products!');
        return;
      });
  }

  handleOpen = name => () => {
    toast.dismiss();
    this.props.show(name);
  };

  formatDate = dateObj => {
    const date = new Date(dateObj);
    const day = date.getUTCDate();
    const month = date.getUTCMonth() + 1;
    const year = date.getUTCFullYear();

    return !this.props.t('language.isRTL')
      ? day + '-' + month + '-' + year
      : toPersianDigits(day.toString()) +
        '-' +
        toPersianDigits(month.toString()) +
        '-' +
        toPersianDigits(year.toString());
  };

  getPrivateCopiedProductPath = (productType, cityName, fileName) => {
    const product = { type: productType, city: cityName, name: fileName };
    const newTabAllowed = window.open('', '_blank');

    return axios
      .post(
        `${process.env
          .REACT_APP_API_URL}/products/downloadPrivateCopiedProduct`,
        product,
        {
          headers: {
            ApiKey: process.env.REACT_APP_API_KEY
          }
        }
      )
      .then(response => {
        const fileName = response.data.substring(
          response.data.lastIndexOf('/') + 1
        );
        const url = `${process.env.REACT_APP_API_URL}${process.env
          .REACT_APP_COPIED_PRODUCTS_URI}/${fileName}`;
        newTabAllowed.location.href = url;

        return response.data;
      })
      .catch(error => {
        console.log(error);
        return false;
      });
  };

  onPrivateToCopyProductClick = (documentURL, productType, cityName) => e => {
    if (!documentURL) {
      //console.log('Error! No document to download');
      return false;
    }
    e.persist();
    e.target.classList.add('faa-flash');
    const fileName = documentURL.substring(documentURL.lastIndexOf('/') + 1);

    this.getPrivateCopiedProductPath(
      productType,
      cityName,
      fileName
    ).then(res => {
      e.target.classList.remove('faa-flash');
      axios
        .post(
          `${process.env
            .REACT_APP_API_URL}/products/deletePrivateCopiedProduct`,
          res,
          {
            headers: {
              ApiKey: process.env.REACT_APP_API_KEY,
              'Content-Type': 'text/plain'
            }
          }
        )
        .then(response => {
          //console.log('Copied product was successfully deleted');
        })
        .catch(error => {
          console.log('Failed to delete private copied product!');
          console.log(error);
        });
    });
  };

  onPublicProductClick = documentURL => () => {
    if (!documentURL) {
      console.log('Error! No document to download');
      return false;
    }
    window.open(`${process.env.REACT_APP_API_URL}${documentURL}`, '_blank');
  };

  handleCarouselItems = products => {
    const { t } = this.props;
    const productsCols = products.map((product, index) => {
      const productImgSrc =
        product.document &&
        !isEmpty(product.document[0]) &&
        !isEmpty(product.document[0].related) &&
        product.document[0].related[0].field === 'image'
          ? `${process.env.REACT_APP_API_URL}${product.document[0].url}`
          : product.document &&
            !isEmpty(product.document[1]) &&
            !isEmpty(product.document[1].related) &&
            product.document[1].related[0].field === 'image'
            ? `${process.env.REACT_APP_API_URL}${product.document[1].url}`
            : require(`../../../img/products/${product.city.pCode}.png`);

      return (
        <MDBCol className='py-2' md='4' key={index}>
          <MDBCard narrow className='mb-2 overlay zoom'>
            <div
              onClick={this.handleZoomImg(
                product.city.pCode,
                product.type.replace(/ /g, '-')
              )}>
              <MDBCardImage
                className={`${product.city.pCode}-${product.type.replace(
                  / /g,
                  '-'
                )}`}
                cascade
                top
                src={productImgSrc}
                alt='products map'
              />
            </div>
            <MDBCardBody cascade>
              <MDBCardTitle className='h4-responsive'>
                <strong className='text-urban-elegant-gray-5'>
                  {t(`urban-cities.${product.city.admin4Name}`)}
                </strong>
              </MDBCardTitle>
              <h6 className='h6-responsive font-smaller text-muted'>
                {t(`urban-products.${product.type}`)}
              </h6>
              <MDBCardFooter
                className={`px-1 d-flex justify-content-between align-items-center pt-2 mt-3 pb-0 ${t(
                  'language.isRTL'
                ) && 'flex-row-reverse'}`}>
                <span className='card-meta text-urban-elegant-gray-5'>
                  {t('home.recent-products.product-slider.last-update')}{' '}
                  {this.formatDate(product.updatedAt)}
                </span>

                {product.type === 'City Fact Sheet' ? (
                  <i
                    className='fa fa-download cursor-pointer text-urban-elegant-gray-5 mx-1'
                    data-tip={t('tooltips.download-product')}
                    onClick={this.onPublicProductClick(
                      product.document &&
                      !isEmpty(product.document[0]) &&
                      !isEmpty(product.document[0].related) &&
                      product.document[0].related[0].field === 'document'
                        ? product.document[0].url
                        : product.document &&
                          !isEmpty(product.document[1]) &&
                          !isEmpty(product.document[1].related) &&
                          product.document[1].related[0].field === 'document'
                          ? product.document[1].url
                          : null
                    )}
                  />
                ) : localStorage.jwtToken ? (
                  this.getProductsPerUserGroup(
                    this.props.auth.user.operations,
                    product
                  )
                ) : (
                  <i
                    className='fa fa-download cursor-default text-urban-elegant-gray-5 opacity-50 mx-1'
                    data-tip={t('tooltips.login-download')}
                  />
                )}
                <ReactTooltip multiline={true} clickable={true} />
              </MDBCardFooter>
            </MDBCardBody>
          </MDBCard>
        </MDBCol>
      );
    });

    const productsItem = [];
    let i = 0;

    while (productsCols.length > 0) {
      let spliced = productsCols.splice(0, carouselSlidesPerRow);
      productsItem.push(
        <MDBCarouselItem className='m-2' itemId={i + 1} key={i}>
          {!isEmpty(spliced) ? spliced : null}
        </MDBCarouselItem>
      );
      i++;
    }
    return productsItem;
  };

  getProductsPerUserGroup = (userGroup, product) => {
    const { t } = this.props;
    if (urbanUserGroups.indexOf(userGroup) > -1) {
      const userGroupAccess =
        userGroup === 'wos'
          ? true
          : urbanUserGroupsList[userGroup].indexOf(product.city.pCode) > -1;
      if (userGroupAccess) {
        return (
          <i
            className='fa fa-download cursor-pointer text-urban-elegant-gray-5 mx-1'
            data-tip={t('tooltips.download-product')}
            onClick={this.onPrivateToCopyProductClick(
              product.document &&
              !isEmpty(product.document[0]) &&
              !isEmpty(product.document[0].related) &&
              product.document[0].related[0].field === 'document'
                ? product.document[0].url
                : product.document &&
                  !isEmpty(product.document[1]) &&
                  !isEmpty(product.document[1].related) &&
                  product.document[1].related[0].field === 'document'
                  ? product.document[1].url
                  : null,
              product.type,
              product.city.admin4Name
            )}
          />
        );
      }
      return (
        <i
          className='fa fa-download cursor-default text-urban-elegant-gray-5 opacity-50 mx-1'
          data-tip={t('tooltips.access-restricted')}
        />
      );
    }
    return (
      <i
        className='fa fa-download cursor-default text-urban-elegant-gray-5 opacity-50 mx-1'
        data-tip={t('tooltips.contact-admin-chk-level')}
      />
    );
  };

  render() {
    const { t, city } = this.props;
    let products = !t('language.isRTL')
      ? this.state.products
      : this.state.productsAr;
    const carouselLength = Math.ceil(products.length / carouselSlidesPerRow);

    return (
      <HashRouter hashType='noslash'>
        <React.Fragment>
          <div id='home-view' dir={`${t('language.isRTL') && 'rtl'}`}>
            <MDBContainer fluid className='text-justify'>
              <MDBModal
                isOpen={this.state.modalDamage}
                toggle={this.toggle}
                size='fluid'
                centered
                className='dmg-assess px-4 modal-notify modal-info text-white font-weight-600'>
                <MDBModalBody>
                  <iframe
                    src={damageAssessmentMapSeries}
                    frameBorder='0'
                    allowFullScreen={true}
                    width='100%'
                    height='100%'
                    title='Damage Assessment Map Series'
                  />
                </MDBModalBody>
              </MDBModal>
            </MDBContainer>
            <View>
              <Mask
                overlay='black-slight'
                className='home-pattern d-flex flex-center'>
                <Container className='px-md-3 px-sm-0 mx-3'>
                  <MDBRow>
                    <div className='d-flex flex-column flex-center mb-2 white-text text-center'>
                      <h1 className='h1-responsive font-weight-500 mb-0 pt-md-5'>
                        {t('home.title.p1')}
                        <span className='text-urban-dark-orange'>
                          {' '}
                          {t('home.title.p2')}
                        </span>
                      </h1>
                      <hr className='hr-light my-4 w-80' />
                      <h4 className='h4-responsive mt-2 mb-4 w-85'>
                        <i
                          className={`fa ${!t('language.isRTL')
                            ? 'fa-quote-left'
                            : 'fa-quote-right'}`}
                          aria-hidden='true'
                        />{' '}
                        {t('home.quote.body')}{' '}
                        <i
                          className={`fa ${!t('language.isRTL')
                            ? 'fa-quote-right'
                            : 'fa-quote-left'}`}
                          aria-hidden='true'
                        />
                      </h4>
                      <span
                        className={`font-italic font-small white-text w-lg-50 w-md-60 w-sm-70 w-80 ${!t(
                          'language.isRTL'
                        )
                          ? 'text-monospace'
                          : 'text-monospaceAr'}`}>
                        {t('home.quote.author')}
                      </span>
                    </div>
                  </MDBRow>
                </Container>
              </Mask>
            </View>
          </div>
          <Container fluid className='home-news'>
            <MDBRow
              center
              className='urban-dark-blue z-depth-2'
              dir={`${t('language.isRTL') && 'rtl'}`}>
              <MDBCol lg='7' md='8' sm='9' size='10'>
                <div className='text-white text-center my-5 mx-2'>
                  <h2 className='h2-responsive font-weight-normal pb-4 text-uppercase'>
                    {t('home.paragraph.title')}
                  </h2>
                  <p
                    className={`font-weight-normal pb-3 ${!t('language.isRTL')
                      ? 'text-justify'
                      : 'text-right'}`}>
                    {t('home.paragraph.body')}
                  </p>

                  <Link to='/about' replace>
                    <MDBBtn className='know-more-btn btn-rounded-urban-dark-orange font-weight-600 btn-sm m-0'>
                      {t('home.know-btn')}
                    </MDBBtn>
                  </Link>
                </div>
              </MDBCol>
            </MDBRow>
            <MDBRow center dir={`${t('language.isRTL') && 'rtl'}`}>
              <MDBCol lg='7' md='8' sm='9' size='10'>
                <div className='text-center m-5'>
                  <h3 className='h3-responsive pb-3'>
                    {t('home.recent-products.paragraph.title')}
                  </h3>
                  <p className='mb-0'>
                    {t('home.recent-products.paragraph.body.p1')}
                  </p>

                  {localStorage.jwtToken ? null : (
                    <p>
                      {t('home.recent-products.paragraph.body.p2')}{' '}
                      <u
                        className='text-urban-dark-orange cursor-pointer'
                        onClick={this.handleOpen('authModal')}>
                        {t('home.recent-products.paragraph.body.p3')}
                      </u>{' '}
                      {t('home.recent-products.paragraph.body.p4')}
                    </p>
                  )}
                </div>
              </MDBCol>
            </MDBRow>
            <MDBRow center>
              <MDBCol
                xl='6'
                lg='8'
                md='10'
                sm='5'
                size='7'
                className='px-0 pb-3'>
                {/*  <MDBCol xl='6' lg='8' md='10' sm='5' size='7' className='px-0'> */}
                <div className='text-center mb-5'>
                  {carouselLength ? (
                    <MDBCarousel
                      activeItem={1}
                      length={carouselLength}
                      slide={true}
                      showControls={true}
                      showIndicators={true}
                      multiItem>
                      <MDBCarouselInner>
                        <MDBRow>{this.handleCarouselItems(products)}</MDBRow>
                      </MDBCarouselInner>
                    </MDBCarousel>
                  ) : null}
                </div>
              </MDBCol>
            </MDBRow>
            <MDBRow center dir={`${t('language.isRTL') && 'rtl'}`}>
              <MDBCol lg='7' md='8' sm='9' size='10'>
                <div className='text-center mx-5 mt-5 mb-4'>
                  <div className='d-flex flex-row align-items-center justify-content-center pb-3 mb-2'>
                    <h3 className='h3-responsive mb-0'>
                      {t('home.damage-assess.paragraph.title')}
                    </h3>
                    <div className='px-2 badge-container'>
                      <span className='badge badge-warning'>
                        {t('home.damage-assess.paragraph.badge')}
                      </span>
                    </div>
                  </div>

                  <p className='mb-0'>
                    {t('home.damage-assess.paragraph.body.p1')}{' '}
                    {localStorage.jwtToken ? (
                      <span>
                        {t('home.damage-assess.paragraph.body.p5')}{' '}
                        <u
                          className='text-urban-dark-orange cursor-pointer'
                          onClick={this.toggle}>
                          {t('home.damage-assess.paragraph.body.p6')}
                        </u>{' '}
                      </span>
                    ) : (
                      <span>
                        {t('home.damage-assess.paragraph.body.p2')}{' '}
                        <u
                          className='text-urban-dark-orange cursor-pointer'
                          onClick={this.handleOpen('authModal')}>
                          {t('home.damage-assess.paragraph.body.p3')}
                        </u>{' '}
                        {t('home.damage-assess.paragraph.body.p4')}
                      </span>
                    )}
                  </p>
                </div>
              </MDBCol>
            </MDBRow>
            <div
              className={`switch-language urban-dark-orange px-3 ${!t(
                'language.isRTL'
              )
                ? 'pt-1 pb-rem-06'
                : 'py-2'} z-depth-1 cursor-pointer`}
              onClick={() => {
                document.title = t('document-title');
                i18next.changeLanguage(t('language.next'));
                const selectValue = city.name;
                if (
                  Array.isArray(selectValue) &&
                  selectValue.length === 1 &&
                  document.getElementsByClassName('css-hcgjr5-singleValue') &&
                  document.getElementsByClassName(
                    'css-hcgjr5-singleValue'
                  )[0] &&
                  document.getElementsByClassName('css-hcgjr5-singleValue')[0]
                    .textContent
                ) {
                  const prevSelectValue = document.getElementsByClassName(
                    'css-hcgjr5-singleValue'
                  )[0].textContent;
                  document.getElementsByClassName(
                    'css-hcgjr5-singleValue'
                  )[0].textContent = t(`select-value.${prevSelectValue}`);
                }
              }}>
              <span
                className={`text-white font-small ${!t('language.isRTL')
                  ? 'font-montserratAr font-weight-normal'
                  : 'font-montserratEn font-weight-500'}`}>
                {`${!t('language.isRTL') ? 'عـربـي' : 'English'}`}
              </span>
            </div>
          </Container>
        </React.Fragment>
      </HashRouter>
    );
  }
}

Home.propTypes = {
  auth: PropTypes.object.isRequired,
  city: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  city: state.city
});

export default connect(mapStateToProps, { show })(withTranslation()(Home));
