import React, { Component, Fragment } from 'react';
import { withTranslation } from 'react-i18next';
import {
  Container,
  Row,
  Col,
  MDBIcon,
  MDBCard,
  MDBCardBody,
  MDBModal,
  MDBModalBody,
  MDBModalHeader
} from 'mdbreact';
import ReactTooltip from 'react-tooltip';
import axios from 'axios';
import CityProfilesMap from '../map/CityProfilesMap';
import PropTypes from 'prop-types';
import { BrowserRouter, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import isEmpty from '../../../utils/is-empty';
import { urbanUserGroups, urbanUserGroupsList } from '../../../json/data.js';

class CityProfiles extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
      interactiveCityProfileURL: null,
      interactiveCityProfileCityName: null
    };
  }
  toggle = () => {
    this.setState({
      modal: !this.state.modal
    });
  };

  onPublicProductClick = documentURL => () => {
    if (!documentURL) {
      console.log('Error! No document to download');
      return false;
    }
    window.open(`${process.env.REACT_APP_API_URL}${documentURL}`, '_blank');
  };

  getPrivateCopiedProductPath = (productType, cityName, fileName) => {
    const product = { type: productType, city: cityName, name: fileName };
    const newTabAllowed = window.open('', '_blank');

    return axios
      .post(
        `${process.env
          .REACT_APP_API_URL}/products/downloadPrivateCopiedProduct`,
        product,
        {
          headers: {
            ApiKey: process.env.REACT_APP_API_KEY
          }
        }
      )
      .then(response => {
        const fileName = response.data.substring(
          response.data.lastIndexOf('/') + 1
        );
        const url = `${process.env.REACT_APP_API_URL}${process.env
          .REACT_APP_COPIED_PRODUCTS_URI}/${fileName}`;
        newTabAllowed.location.href = url;

        return response.data;
      })
      .catch(error => {
        console.log(error);
        return false;
      });
  };

  onPrivateToCopyProductClick = (documentURL, productType, cityName) => e => {
    if (!documentURL) {
      //console.log('Error! No document to download');
      return false;
    }

    if (productType !== 'Interactive City Profile') {
      e.persist();
      const targetElement = e.currentTarget.querySelector('i');
      targetElement.classList.add('faa-flash');
      const fileName = documentURL.substring(documentURL.lastIndexOf('/') + 1);

      this.getPrivateCopiedProductPath(
        productType,
        cityName,
        fileName
      ).then(res => {
        targetElement.classList.remove('faa-flash');
        axios
          .post(
            `${process.env
              .REACT_APP_API_URL}/products/deletePrivateCopiedProduct`,
            res,
            {
              headers: {
                ApiKey: process.env.REACT_APP_API_KEY,
                'Content-Type': 'text/plain'
              }
            }
          )
          .then(response => {
            //console.log('Copied product was successfully deleted');
          })
          .catch(error => {
            console.log('Failed to delete private copied product!');
            console.log(error);
          });
      });
    }
    else {
      this.setState({
        interactiveCityProfileURL: documentURL,
        interactiveCityProfileCityName: cityName
      });
      this.toggle();
    }
  };

  getProductsPerUserGroup = (
    pcode,
    userGroup,
    productAccess,
    products,
    productName,
    productTitle,
    productColor,
    cityName,
    productDesc
  ) => {
    const { t } = this.props;
    if (urbanUserGroups.indexOf(userGroup) > -1) {
      const userGroupAccess =
        userGroup === 'wos'
          ? true
          : urbanUserGroupsList[userGroup].indexOf(pcode) > -1;
      if (userGroupAccess) {
        return (
          productAccess && (
            <MDBCard
              data-tip={productDesc}
              text='white'
              className={`${productColor} text-center m-2 cursor-pointer mw-40 w-auto`}>
              <MDBCardBody
                className='px-3 py-2'
                onClick={this.onPrivateToCopyProductClick(
                  products,
                  productName,
                  cityName
                )}>
                <p className='font-smaller mb-1'>{productTitle}</p>
                <MDBIcon id={`cp-i-${cityName}`} icon='download' size='sm' />
              </MDBCardBody>
            </MDBCard>
          )
        );
      }
      return (
        productAccess && (
          <MDBCard
            data-tip={t('tooltips.access-restricted')}
            text='white'
            className={`${productColor} text-center m-2 cursor-default mw-40 w-auto`}>
            <MDBCardBody
              className='px-3 py-2'
              onClick={this.onPrivateToCopyProductClick(
                null,
                productName,
                cityName
              )}>
              <p className='font-smaller mb-1'>{productTitle}</p>
              <MDBIcon
                className='cursor-default opacity-50'
                icon='download'
                size='sm'
              />
            </MDBCardBody>
          </MDBCard>
        )
      );
    }
    return (
      productAccess && (
        <MDBCard
          data-tip={t('tooltips.contact-admin-chk-level')}
          text='white'
          className={`${productColor} text-center m-2 cursor-default mw-40 w-auto`}>
          <MDBCardBody
            className='px-3 py-2'
            onClick={this.onPrivateToCopyProductClick(
              null,
              productName,
              cityName
            )}>
            <p className='font-smaller mb-1'>{productTitle}</p>
            <MDBIcon
              className='cursor-default opacity-50'
              icon='download'
              size='sm'
            />
          </MDBCardBody>
        </MDBCard>
      )
    );
  };

  render() {
    const { city, errors } = this.props;
    const {
      interactiveCityProfileURL,
      interactiveCityProfileCityName
    } = this.state;
    const { t } = this.props;

    return (
      <BrowserRouter>
        <Container
          fluid
          className={`${!t('language.isRTL') ? 'text-justify' : 'text-right'}`}>
          <MDBModal
            isOpen={this.state.modal}
            toggle={this.toggle}
            size='fluid'
            centered
            className='icp-modal px-4 modal-notify modal-info text-white font-weight-600'>
            <MDBModalHeader
              className={`d-flex align-items-center justify-content-between ${!t(
                'language.isRTL'
              )
                ? 'flex-row'
                : 'flex-row-reverse'}`}
              toggle={this.toggle}>
              {interactiveCityProfileCityName && (
                <div className='d-flex flex-row align-items-center justify-content-center'>
                  <span className='px-1'>
                    {t(`urban-cities.${interactiveCityProfileCityName}`)}
                  </span>{' '}
                  <span> {t('city-profiles.interactive-profile-in')}</span>
                </div>
              )}
            </MDBModalHeader>
            <MDBModalBody>
              <iframe
                src={interactiveCityProfileURL}
                frameBorder='0'
                allowFullScreen={true}
                width='100%'
                height='100%'
                title='Interactive Profile'
              />
            </MDBModalBody>
          </MDBModal>
          <Row
            center
            className='urban-dark-blue z-depth-2 green'
            dir={`${t('language.isRTL') && 'rtl'}`}>
            <Col lg='7' md='8' sm='9' size='10'>
              <div className='text-white my-5 mx-2'>
                <h2 className='h2-responsive text-center font-weight-bold pb-4 text-uppercase'>
                  {t('city-profiles.paragraph.title')}
                </h2>
                <p className='font-weight-normal'>
                  {t('city-profiles.paragraph.body.p1')}
                </p>
                <div className='text-white mb-5 mx-2'>
                  <ul
                    className={`list-unstyled ml-3 pb-1 ${!t(
                      'language.isRTL'
                    ) && 'text-left'}`}>
                    <li>
                      <p className='mb-2'>
                        <MDBIcon
                          fixed
                          icon='check-square'
                          className='mx-1 font-small'
                        />
                        {t('city-profiles.paragraph.body.p2.i1')}
                      </p>
                    </li>
                    <li>
                      <p className='mb-2'>
                        <MDBIcon
                          fixed
                          icon='check-square'
                          className='mx-1 font-small'
                        />
                        {t('city-profiles.paragraph.body.p2.i2')}
                      </p>
                    </li>
                    <li>
                      <p className='mb-2'>
                        <MDBIcon
                          fixed
                          icon='check-square'
                          className='mx-1 font-small'
                        />
                        {t('city-profiles.paragraph.body.p2.i3')}
                      </p>
                    </li>
                    <li>
                      <p className='mb-2'>
                        <MDBIcon
                          fixed
                          icon='check-square'
                          className='mx-1 font-small'
                        />
                        {t('city-profiles.paragraph.body.p2.i4')}
                      </p>
                    </li>
                  </ul>
                </div>
              </div>
            </Col>
          </Row>

          <Row center className='z-depth-2'>
            <Col lg='8' size='12' className='p-0'>
              <CityProfilesMap />
            </Col>
            <Col
              lg='4'
              size='12'
              className='d-flex flex-column justify-content-center'>
              {!isEmpty(errors.city) ? (
                <div className='flex-center font-weight-500 my-5'>
                  <MDBCard
                    text='white'
                    className='urban-elegant-gray-7-opacity text-center m-2 cursor-default mw-40 w-50'>
                    <MDBCardBody className='p-3'>
                      <p className='font-small mb-0'>
                        {t(`msgs.${errors.city}`)}
                      </p>
                    </MDBCardBody>
                  </MDBCard>
                </div>
              ) : (
                city.pcode.map((pcode, i) => {
                  return (
                    <div className='text-center' key={pcode}>
                      <div>
                        <h5 className='h5-responsive pt-1 px-1 pb-0 m-0 font-weight-normal'>
                          {t(`urban-cities.${city.name[i]}`)}
                        </h5>
                      </div>
                      {isEmpty(city.products[i]) &&
                      !isEmpty(city.errors[i]) &&
                      Object.values(city.errors[i]).every(e => e === 404) ? (
                        <div className='text-center mb-2'>
                          <div className='font-small text-center px-5'>
                            {t('msgs.no-city-products')}
                            <br /> {t('msgs.check-soon')}
                          </div>
                        </div>
                      ) : (
                        <div>
                          <ul className='pl-0 mb-2 d-flex flex-wrap justify-content-center align-items-start'>
                            {!isEmpty(city.products[i]) &&
                            !isEmpty(city.products[i].cityFactSheet) && (
                              <MDBCard
                                data-tip={t('tooltips.fact-sheet-tip')}
                                text='white'
                                className='urban-dark-orange-opacity text-center m-2 cursor-pointer mw-40 w-auto'>
                                <MDBCardBody
                                  className='px-3 py-2'
                                  onClick={this.onPublicProductClick(
                                    !t('language.isRTL')
                                      ? city.products[i].cityFactSheet.en
                                      : city.products[i].cityFactSheet.ar
                                        ? city.products[i].cityFactSheet.ar
                                        : city.products[i].cityFactSheet.en
                                  )}>
                                  <span
                                    className={`font-smaller mb-1 d-flex ${!t(
                                      'language.isRTL'
                                    )
                                      ? 'flex-column'
                                      : 'flex-column-reverse'}`}>
                                    <span>{t('urban-products.fact')}</span>
                                    <span>{t('urban-products.sheet')}</span>
                                  </span>
                                  <MDBIcon
                                    id={`cf-i-${city.name[i]}`}
                                    icon='download'
                                    size='sm'
                                  />
                                </MDBCardBody>
                              </MDBCard>
                            )}
                            {isEmpty(localStorage.jwtToken) ? (
                              !isEmpty(city.products[i]) &&
                              city.products[i].cityProfileReport === true && (
                                <MDBCard
                                  data-tip={t('tooltips.login-download')}
                                  text='white'
                                  className='urban-dark-blue-opacity text-center m-2 cursor-default mw-40 w-auto'>
                                  <MDBCardBody
                                    className='px-3 py-2'
                                    onClick={this.onPrivateToCopyProductClick(
                                      null,
                                      'City Profile Report',
                                      city.name[i]
                                    )}>
                                    <span
                                      className={`font-smaller mb-1 d-flex ${!t(
                                        'language.isRTL'
                                      )
                                        ? 'flex-column'
                                        : 'flex-column-reverse'}`}>
                                      <span>{t('urban-products.profile')}</span>
                                      <span>{t('urban-products.report')}</span>
                                    </span>
                                    <MDBIcon
                                      className='cursor-default opacity-50'
                                      icon='download'
                                      size='sm'
                                    />
                                  </MDBCardBody>
                                </MDBCard>
                              )
                            ) : (
                              this.getProductsPerUserGroup(
                                pcode,
                                this.props.auth.user.operations,
                                !isEmpty(city.products[i]) &&
                                  !isEmpty(city.products[i].cityProfileReport),
                                city.products[i].cityProfileReport,
                                'City Profile Report',
                                <span
                                  className={`d-flex ${!t('language.isRTL')
                                    ? 'flex-column'
                                    : 'flex-column-reverse'}`}>
                                  <span>{t('urban-products.profile')}</span>
                                  <span>{t('urban-products.report')}</span>
                                </span>,
                                'urban-dark-blue-opacity',
                                city.name[i],
                                t('tooltips.profile-report-tip')
                              )
                            )}
                            {isEmpty(localStorage.jwtToken) ? (
                              !isEmpty(city.products[i]) &&
                              city.products[i].interactiveCityProfile ===
                                true && (
                                <MDBCard
                                  data-tip={t('tooltips.login-view')}
                                  text='white'
                                  className='urban-elegant-gray-7-opacity text-center m-2 cursor-default mw-40 w-auto'>
                                  <MDBCardBody
                                    className='px-3 py-2'
                                    onClick={this.onPrivateToCopyProductClick(
                                      null,
                                      'Interactive City Profile',
                                      city.name[i]
                                    )}>
                                    <span
                                      className={`font-smaller mb-1 d-flex ${!t(
                                        'language.isRTL'
                                      )
                                        ? 'flex-column'
                                        : 'flex-column-reverse'}`}>
                                      <span>
                                        {t('urban-products.interactive')}
                                      </span>
                                      <span>{t('urban-products.profile')}</span>
                                    </span>
                                    <MDBIcon
                                      className='cursor-default opacity-50'
                                      icon='download'
                                      size='sm'
                                    />
                                  </MDBCardBody>
                                </MDBCard>
                              )
                            ) : (
                              this.getProductsPerUserGroup(
                                pcode,
                                this.props.auth.user.operations,
                                !isEmpty(city.products[i]) &&
                                  !isEmpty(
                                    city.products[i].interactiveCityProfile
                                  ),
                                city.products[i].interactiveCityProfile,
                                'Interactive City Profile',
                                <span
                                  className={`d-flex ${!t('language.isRTL')
                                    ? 'flex-column'
                                    : 'flex-column-reverse'}`}>
                                  <span>{t('urban-products.interactive')}</span>
                                  <span>{t('urban-products.profile')}</span>
                                </span>,
                                'urban-elegant-gray-7-opacity',
                                city.name[i],
                                t('tooltips.interactive-profile-tip')
                              )
                            )}
                            {isEmpty(localStorage.jwtToken) ? (
                              !isEmpty(city.products[i]) &&
                              city.products[i].dataSets === true && (
                                <MDBCard
                                  data-tip={t('tooltips.login-download')}
                                  text='white'
                                  className='urban-elegant-red-7-opacity text-center m-2 cursor-default mw-40 w-auto'>
                                  <MDBCardBody
                                    className='px-3 py-2'
                                    onClick={this.onPrivateToCopyProductClick(
                                      null,
                                      'Datasets',
                                      city.name[i]
                                    )}>
                                    <p className='font-smaller mb-1'>
                                      {t('urban-products.datasets')}
                                    </p>
                                    <MDBIcon
                                      className='cursor-default opacity-50'
                                      icon='download'
                                      size='sm'
                                    />
                                  </MDBCardBody>
                                </MDBCard>
                              )
                            ) : (
                              this.getProductsPerUserGroup(
                                pcode,
                                this.props.auth.user.operations,
                                !isEmpty(city.products[i]) &&
                                  !isEmpty(city.products[i].dataSets),
                                city.products[i].dataSets,
                                'Datasets',
                                <Fragment>
                                  {t('urban-products.datasets')}
                                </Fragment>,
                                'urban-elegant-red-7-opacity',
                                city.name[i],
                                t('tooltips.datasets-tip')
                              )
                            )}
                            {!isEmpty(city.products[i]) &&
                            !isEmpty(city.products[i].urbanBaseline) && (
                              <MDBCard
                                data-tip={t('tooltips.urban-baseline-tip')}
                                text='white'
                                className='urban-darker-blue-opacity text-center m-2 cursor-pointer mw-40 w-auto'>
                                <MDBCardBody
                                  className='px-3 py-2'
                                  onClick={this.onPublicProductClick(
                                    !t('language.isRTL')
                                      ? city.products[i].urbanBaseline.en
                                      : city.products[i].urbanBaseline.ar
                                        ? city.products[i].urbanBaseline.ar
                                        : city.products[i].urbanBaseline.en
                                  )}>
                                  <span
                                    className={`font-smaller mb-1 d-flex ${!t(
                                      'language.isRTL'
                                    )
                                      ? 'flex-column'
                                      : 'flex-column-reverse'}`}>
                                    <span>{t('urban-products.urban')}</span>
                                    <span>{t('urban-products.baseline')}</span>
                                  </span>
                                  <MDBIcon
                                    id={`cf-i-${city.name[i]}`}
                                    icon='download'
                                    size='sm'
                                  />
                                </MDBCardBody>
                              </MDBCard>
                            )}
                          </ul>
                          <ReactTooltip multiline={true} clickable={true} />
                        </div>
                      )}
                    </div>
                  );
                })
              )}
            </Col>
          </Row>
        </Container>
      </BrowserRouter>
    );
  }
}

CityProfiles.propTypes = {
  city: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  city: state.city,
  auth: state.auth,
  errors: state.errors
});

export default connect(mapStateToProps, {})(
  withRouter(withTranslation()(CityProfiles))
);
