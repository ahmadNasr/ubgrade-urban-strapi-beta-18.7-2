import {
  Col,
  Container,
  MDBBtn,
  MDBCard,
  MDBCardBody,
  MDBCol,
  MDBContainer,
  MDBRow,
  Row
} from 'mdbreact';
import React, { Component } from 'react';

import ReactTooltip from 'react-tooltip';
import axios from 'axios';
import toolkitHow from '../../../img/toolkit-how.png';
import toolkitHowAr from '../../../img/ar/toolkit-how.png';
import { withTranslation } from 'react-i18next';

class Toolkit extends Component {
  componentDidMount() {
    setTimeout(() => {
      ReactTooltip.rebuild();
    }, 100);
  }

  onToolkitProductClick = type => () => {
    const newTabAllowed = window.open('', '_blank');
    axios
      .get(
        `${process.env.REACT_APP_API_URL}/toolkits/getToolkitProduct/${type}`,
        {
          headers: {
            ApiKey: process.env.REACT_APP_API_KEY
          }
        }
      )
      .then(response => {
        const documentURL = response.data;
        if (!documentURL) {
          console.log(`No available URL for the Toolkit ${type} document!`);
          return false;
        }
        newTabAllowed.location.href = `${process.env
          .REACT_APP_API_URL}${documentURL}`;
      })
      .catch(error => {
        console.log(`No available document uploaded for the Toolkit ${type}!`);
        console.log(error);
      });
  };

  render() {
    const { t } = this.props;
    return (
      <Container
        fluid
        className={`${!t('language.isRTL') ? 'text-justify' : 'text-right'}`}
        dir={`${t('language.isRTL') && 'rtl'}`}>
        <Row center className='urban-elegant-gray-4 z-depth-2'>
          <Col lg='7' md='8' sm='9' size='10'>
            <div className='text-white my-5 mx-2'>
              <h2 className='h2-responsive text-center font-weight-bold pb-4 text-uppercase'>
                {t('toolkit.paragraph.title')}
              </h2>
              <p className='font-weight-normal pb-3'>
                {t('toolkit.paragraph.body.p1')}
              </p>
              <p className='font-weight-normal pb-3'>
                {t('toolkit.paragraph.body.p2')}
              </p>
              <p className='font-weight-normal pb-3'>
                {t('toolkit.paragraph.body.p3')}
              </p>

              <div className='p-0 m-0 text-center'>
                <p className='font-weight-normal pb-3'>
                  {t('toolkit.paragraph.body.p4')}
                </p>
                <MDBBtn
                  className='know-more-btn btn-rounded-urban-original-orange font-weight-600 btn-md'
                  onClick={this.onToolkitProductClick('methodology')}>
                  {t('toolkit.methodology-btn')}
                  <i
                    className={`fa fa-download ${!t('language.isRTL')
                      ? 'ml-2'
                      : 'mr-2'}`}
                  />
                </MDBBtn>
              </div>
              <p className='font-weight-normal py-3 mt-3'>
                {t('toolkit.paragraph.body.p5')}
              </p>
              <div className='text-center'>
                <img
                  src={!t('language.isRTL') ? toolkitHow : toolkitHowAr}
                  className='toolkit-how-img mt-4 mb-5'
                  alt='Toolkit How'
                />
              </div>

              <MDBContainer className='mx-auto px-auto'>
                <MDBRow className='pt-2 pb-0 pb-lg-2'>
                  <MDBCol className='d-flex flex-column flex-lg-row justify-content-center align-items-center'>
                    <MDBCard
                      text='white'
                      className='text-center m-2 w-lg-50 w-md-60 w-sm-70 w-auto h-lg-100 backgroundcolor-toolkit-adapt'>
                      <MDBCardBody className='p-4'>
                        <div className='w-100 h-100 d-flex flex-column align-items-center justify-content-between p-2'>
                          <h6 className='h6-responsive font-weight-500 pb-2'>
                            {t('toolkit.tools.adapt.title')}
                          </h6>
                          <p className='p-2'>{t('toolkit.tools.adapt.body')}</p>
                          <i
                            data-tip={t('tooltips.download-tool')}
                            className='fa fa-download fa-2x p-1 cursor-pointer'
                            onClick={this.onToolkitProductClick('adapt')}
                          />
                        </div>
                      </MDBCardBody>
                    </MDBCard>
                    <MDBCard
                      text='white'
                      className='text-center m-2 w-lg-50 w-md-60 w-sm-70 w-auto h-lg-100 backgroundcolor-toolkit-collect'>
                      <MDBCardBody className='p-4'>
                        <div className='w-100 h-100 d-flex flex-column align-items-center justify-content-between p-2'>
                          <h6 className='h6-responsive font-weight-500 pb-2'>
                            {t('toolkit.tools.collect.title')}
                          </h6>
                          <p className='p-2'>
                            {t('toolkit.tools.collect.body')}
                          </p>
                          <i
                            data-tip={t('tooltips.download-tool')}
                            className='fa fa-download fa-2x p-1 cursor-pointer'
                            onClick={this.onToolkitProductClick('collect')}
                          />
                        </div>
                      </MDBCardBody>
                    </MDBCard>
                  </MDBCol>
                </MDBRow>
                <MDBRow className='pb-2 pt-0 pt-lg-2'>
                  <MDBCol className='d-flex flex-column flex-lg-row justify-content-center align-items-center'>
                    <MDBCard
                      className='text-urban-elegant-gray-5 text-center m-2 w-lg-50 w-md-60 w-sm-70 w-auto h-lg-100 backgroundcolor-toolkit-analyse'>
                      <MDBCardBody className='p-4'>
                        <div className='w-100 h-100 d-flex flex-column align-items-center justify-content-between p-2'>
                          <h6 className='h6-responsive font-weight-500 pb-2'>
                            {t('toolkit.tools.analyse.title')}
                          </h6>
                          <p className='p-2'>
                            {t('toolkit.tools.analyse.body')}
                          </p>
                          <i
                            data-tip={t('tooltips.download-tool')}
                            className='fa fa-download fa-2x p-1 cursor-pointer'
                            onClick={this.onToolkitProductClick('analyse')}
                          />
                        </div>
                      </MDBCardBody>
                    </MDBCard>
                    <MDBCard
                      className='text-urban-elegant-gray-5 text-center m-2 w-lg-50 w-md-60 w-sm-70 w-auto h-lg-100 backgroundcolor-toolkit-use'>
                      <MDBCardBody className='p-4'>
                        <div className='w-100 h-100 d-flex flex-column align-items-center justify-content-between p-2'>
                          <h6 className='h6-responsive font-weight-500 pb-2'>
                            {t('toolkit.tools.use.title')}
                          </h6>
                          <p className='p-2'>
                            {t('toolkit.tools.use.body')}
                          </p>
                          <i
                            data-tip={t('tooltips.download-tool')}
                            className='fa fa-download fa-2x p-1 cursor-pointer'
                            onClick={this.onToolkitProductClick('use')}
                          />
                        </div>
                      </MDBCardBody>
                    </MDBCard>
                  </MDBCol>
                </MDBRow>
              </MDBContainer>
              <ReactTooltip multiline={true} clickable={true} />
            </div>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default withTranslation()(Toolkit);
