import React, { Component } from 'react';
//import './extensions/fullpage.resetSliders.min.js';
//import './extensions/fullpage.parallax.min.js';
import ReactFullpage from '@fullpage/react-fullpage';
import Home from './sections/Home';
import Approach from './sections/Approach';
import CityProfiles from './sections/CityProfiles';
import Toolkit from './sections/Toolkit';
import About from './sections/About';
import Contact from './sections/Contact';

class FullpageWrapper extends Component {
  render() {
    return (
      <ReactFullpage
        anchors={[
          'home',
          'approach',
          'city-profiles',
          'toolkit',
          'about',
          'contact'
        ]}
        menu={'.navbar'}
        fixedElements={'.navbar'}
        autoScrolling={false}
        fitToSection={false}
        licenseKey={'02514F97-A6FB4C95-825C4BA3-4273267D'}
        /*         resetSliders={true}
        resetSlidersKey={'5E1B1F90-7C6E42B1-8AFB1EB4-9D66C9C5'} */
        /* parallax={true}
        parallaxOptions={
          ({ type: 'reveal' }, { percentage: 100 }, { property: 'background' })
        } */
        render={({ state, fullpageApi }) => {
          return (
            <ReactFullpage.Wrapper>
              <div
                data-anchor='home'
                className='section home urban-slight-gray'>
                <div className='fp-bg' />
                <Home />
              </div>
              <div
                data-anchor='approach'
                className='section approach urban-slight-gray pt-rem-3'>
                <Approach />
              </div>
              <div
                data-anchor='city-profiles'
                className='section city-profiles urban-slight-gray pt-2'>
                <CityProfiles />
              </div>
              <div
                data-anchor='toolkit'
                className='section toolkit urban-slight-gray pt-rem-3'>
                <Toolkit />
              </div>
              <div
                data-anchor='about'
                className='section urban-elegant-gray-4 pt-rem-3'>
                <About />
              </div>
              <div
                data-anchor='contact'
                className='section contact urban-slight-gray'>
                <Contact />
              </div>
            </ReactFullpage.Wrapper>
          );
        }}
      />
    );
  }
}

export default FullpageWrapper;
