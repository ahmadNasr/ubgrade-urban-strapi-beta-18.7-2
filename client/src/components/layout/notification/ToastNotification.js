import React, { Component } from 'react';
//import PropTypes from 'prop-types';
import { withTranslation } from 'react-i18next';
//import { connect } from 'react-redux';
//import { show } from 'redux-modal';
//import { MDBIcon, MDBBtn, toast } from 'mdbreact';
import { MDBIcon } from 'mdbreact';

class ToastNotification extends Component {
  /* constructor(props) {
    super(props);
    this.state = {};
  }
   handleOpen = name => () => {
    //this.props.show(name);
    window.open(
      'https://docs.google.com/forms/d/e/1FAIpQLSeiJJB2hORUzulxHK4tm2zyV2c8OCvDA39YSMP_70lvfzId0w/viewform?usp=sf_link',
      '_blank'
    );
    toast.dismiss();
  };

  closeToast = () => {
    toast.dismiss();
  }; */

  render() {
    const { t } = this.props;
    return (
      /* Training Cancellation Toast Start */
      <div
        className={`d-flex flex-column flex-center text-center small ${!t(
          'language.isRTL'
        )
          ? 'font-montserratEn font-smaller'
          : 'font-montserratAr font-smaller-1'}`}>
        <div className='mt-3 mb-3'>
          <MDBIcon
            className='fa-4x my-2 faa-flash-1'
            icon='exclamation-triangle'
          />
        </div>
        <div className='mx-4 mt-1 mb-3'>
          <span className='font-weight-500'>
            {t('toast.training-cancel-msg-p1')}
            <br />
            {t('toast.training-cancel-msg-p2')}
          </span>
          <br />
          <br />
          <span className='font-weight-500'>
            {t('toast.training-cancel-msg-p3')}
            <br />
            {t('toast.training-cancel-msg-p4')}
            <br />
            {t('toast.training-cancel-msg-p5')}
            <br />
            {t('toast.training-cancel-msg-p6')}
            <br />
            {t('toast.training-cancel-msg-p7')}
            <br />
            {t('toast.training-cancel-msg-p8')}
            <br />
            {t('toast.training-cancel-msg-p9')}
          </span>
        </div>
      </div>
      /* Training Cancellation Toast End */

      /* Training Toast Start */
      /*       <div
        className={`d-flex flex-column flex-center text-center small font-smaller-1 pl-2 ${!t(
          'language.isRTL'
        )
          ? 'font-montserratEn'
          : 'font-montserratAr'}`}>
        <div className='mt-3 mb-2'>
          <MDBIcon className='fa-4x my-2' icon='calendar-alt' />
        </div>
        <div className='mx-4 my-1'>
          <span className='font-weight-500'>{t('toast.training-msg-p1')}</span>
          <br />
          <span className='font-weight-500'>
            {t('toast.training-msg-p2')}
            <br />
            {t('toast.training-msg-p3')}
            <br />
            {t('toast.training-msg-p4')}
          </span>
        </div>

        <div
          className={`d-flex align-items-center justify-content-center ${t(
            'language.isRTL'
          ) && 'flex-row-reverse'}`}>
          <MDBBtn
            className='font-weight-600 white-text my-3'
            outline
            color='white'
            size='sm'
            onClick={this.closeToast}>
            {t('toast.no-thanks')}
          </MDBBtn>
          <MDBBtn
            className='font-weight-600 my-3 text-training'
            color='white'
            size='sm'
            onClick={this.handleOpen('authModal')}>
            {t('toast.register')}
          </MDBBtn>
        </div>
      </div> */
      /* Training Toast End */

      /* Register Toast Start */
      /*       <div
        className={`d-flex flex-column flex-center text-center small font-smaller-2 pl-2 ${!t(
          'language.isRTL'
        )
          ? 'font-montserratEn'
          : 'font-montserratAr'}`}>
        <div className='mt-3 mb-2'>
          <MDBIcon className='fa-4x my-2' icon='bullhorn' />
        </div>
        <div className='mx-4 my-1'>
          <p className='font-weight-500'>
            {t('toast.msg-p1')} <br /> {t('toast.msg-p2')}
          </p>
        </div>

        <div className='d-flex flex-row align-items-center justify-content-center'>
          <MDBBtn
            className='font-weight-500 white-text my-3'
            outline
            color='white'
            size='sm'
            onClick={this.closeToast}>
            {t('toast.no-thanks')}
          </MDBBtn>
          <MDBBtn
            className='font-weight-500 my-3'
            color='white'
            size='sm'
            onClick={this.handleOpen('authModal')}>
            {t('forms.login')}
          </MDBBtn>
        </div>
      </div> */
      /* Register Toast End */

      /* NDA Toast Start */
      /* <div
        className={`d-flex flex-column flex-center text-center small font-smaller-1 pl-2 ${!t(
          'language.isRTL'
        )
          ? 'font-montserratEn'
          : 'font-montserratAr'}`}>
        <div className='mt-3 mb-2'>
          <MDBIcon className='fa-4x my-2' icon='map-marked-alt' />
        </div>
        <div className='mx-4 my-1'>
          <span className='font-weight-500'>{t('toast.nda-msg-p1')}</span>
          <br />
          <span className='font-weight-500'>{t('toast.nda-msg-p2')}</span>
          <br />
          <span className='font-weight-500'>{t('toast.nda-msg-p3')}</span>
        </div>

        <div
          className={`d-flex align-items-center justify-content-center ${t(
            'language.isRTL'
          ) && 'flex-row-reverse'}`}>
          <MDBBtn
            className='font-weight-600 my-3 text-training'
            color='white'
            size='sm'
            onClick={this.handleOpen('authModal')}>
            {t('forms.login')}
          </MDBBtn>
          <MDBBtn
            className='font-weight-600 white-text my-3'
            outline
            color='white'
            size='sm'
            onClick={this.closeToast}>
            {t('toast.no-thanks')}
          </MDBBtn>
        </div>
      </div> */
      /* NDA Toast End */
    );
  }
}

/* ToastNotification.propTypes = {
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(mapStateToProps, { show })(
  withTranslation()(ToastNotification)
); */

export default withTranslation()(ToastNotification);
