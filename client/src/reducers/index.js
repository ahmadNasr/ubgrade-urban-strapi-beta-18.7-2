import { combineReducers } from 'redux';
import { reducer as modal } from 'redux-modal';
import authReducer from './authReducer';
import errorReducer from './errorReducer';
import selectReducer from './selectReducer';

const rootReducer = combineReducers({
  modal,
  auth: authReducer,
  errors: errorReducer,
  city: selectReducer
});

export default rootReducer;
