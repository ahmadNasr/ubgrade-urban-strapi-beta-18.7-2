export const idleb = [
  {
    value: [36.635276032000036, 35.9290080450001],
    label: 'Idleb',
    admin4Pcode: 'C3871',
    admin1Name: 'Idleb',
    admin1Pcode: 'SY07'
  },
  {
    value: [36.3149531, 35.814180826],
    label: 'Jisr-Ash-Shugur',
    admin4Pcode: 'C4199',
    admin1Name: 'Idleb',
    admin1Pcode: 'SY07'
  }
];

export const idlebAr = [
  {
    value: [36.635276032000036, 35.9290080450001],
    label: 'إدلب',
    admin4Pcode: 'C3871',
    admin1Name: 'Idleb',
    admin1Pcode: 'SY07'
  },
  {
    value: [36.3149531, 35.814180826],
    label: 'جسر الشغور',
    admin4Pcode: 'C4199',
    admin1Name: 'Idleb',
    admin1Pcode: 'SY07'
  }
];

export const arRaqqa = [
  {
    value: [39.00606189700011, 35.95307453000015],
    label: 'Ar-Raqqa',
    admin4Pcode: 'C5710',
    admin1Name: 'Ar-Raqqa',
    admin1Pcode: 'SY11'
  }
];

export const arRaqqaAr = [
  {
    value: [39.00606189700011, 35.95307453000015],
    label: 'الرقة',
    admin4Pcode: 'C5710',
    admin1Name: 'Ar-Raqqa',
    admin1Pcode: 'SY11'
  }
];

export const damascus = [
  {
    value: [36.3325752124, 33.523466849],
    label: 'Jawbar',
    admin4Pcode: 'C6001',
    admin1Name: 'Damascus',
    admin1Pcode: 'SY01'
  },
  {
    value: [36.3381133037, 33.543480566],
    label: 'Qaboun',
    admin4Pcode: 'C6002',
    admin1Name: 'Damascus',
    admin1Pcode: 'SY01'
  }
];

export const damascusAr = [
  {
    value: [36.3325752124, 33.523466849],
    label: 'جوبر',
    admin4Pcode: 'C6001',
    admin1Name: 'Damascus',
    admin1Pcode: 'SY01'
  },
  {
    value: [36.3381133037, 33.543480566],
    label: 'القابون',
    admin4Pcode: 'C6002',
    admin1Name: 'Damascus',
    admin1Pcode: 'SY01'
  }
];

export const ruralDamascus = [
  {
    value: [36.311829455, 33.604157399],
    label: 'At Tall',
    admin4Pcode: 'C2395',
    admin1Name: 'Rural Damascus',
    admin1Pcode: 'SY03'
  },
  {
    value: [36.40459234100007, 33.57057203900018],
    label: 'Duma',
    admin4Pcode: 'C2338',
    admin1Name: 'Rural Damascus',
    admin1Pcode: 'SY03'
  },
  {
    value: [36.34831672200005, 33.48765732600014],
    label: 'Jaramana',
    admin4Pcode: 'C2304',
    admin1Name: 'Rural Damascus',
    admin1Pcode: 'SY03'
  },
  {
    value: [36.21734372400016, 33.54819815500008],
    label: 'Qudsiya',
    admin4Pcode: 'C2328',
    admin1Name: 'Rural Damascus',
    admin1Pcode: 'SY03'
  }
];

export const ruralDamascusAr = [
  {
    value: [36.311829455, 33.604157399],
    label: 'التل',
    admin4Pcode: 'C2395',
    admin1Name: 'Rural Damascus',
    admin1Pcode: 'SY03'
  },
  {
    value: [36.40459234100007, 33.57057203900018],
    label: 'دوما',
    admin4Pcode: 'C2338',
    admin1Name: 'Rural Damascus',
    admin1Pcode: 'SY03'
  },
  {
    value: [36.34831672200005, 33.48765732600014],
    label: 'جرمانا',
    admin4Pcode: 'C2304',
    admin1Name: 'Rural Damascus',
    admin1Pcode: 'SY03'
  },
  {
    value: [36.21734372400016, 33.54819815500008],
    label: 'قدسيا',
    admin4Pcode: 'C2328',
    admin1Name: 'Rural Damascus',
    admin1Pcode: 'SY03'
  }
];

export const hama = [
  {
    value: [37.051036244, 35.011318683],
    label: 'As-Salamiyeh',
    admin4Pcode: 'C3244',
    admin1Name: 'Hama',
    admin1Pcode: 'SY05'
  },
  {
    value: [36.76129068400007, 35.133741730000054],
    label: 'Hama',
    admin4Pcode: 'C2987',
    admin1Name: 'Hama',
    admin1Pcode: 'SY05'
  }
];

export const hamaAr = [
  {
    value: [37.051036244, 35.011318683],
    label: 'السلمية',
    admin4Pcode: 'C3244',
    admin1Name: 'Hama',
    admin1Pcode: 'SY05'
  },
  {
    value: [36.76129068400007, 35.133741730000054],
    label: 'حماة',
    admin4Pcode: 'C2987',
    admin1Name: 'Hama',
    admin1Pcode: 'SY05'
  }
];

export const homs = [
  {
    value: [36.57991039900003, 34.509591411000144],
    label: 'Al-Qusayr',
    admin4Pcode: 'C2733',
    admin1Name: 'Homs',
    admin1Pcode: 'SY04'
  },
  {
    value: [36.731226002000085, 34.92409498800009],
    label: 'Ar-Rastan',
    admin4Pcode: 'C2870',
    admin1Name: 'Homs',
    admin1Pcode: 'SY04'
  },
  {
    value: [36.71973076900014, 34.72996072899997],
    label: 'Homs',
    admin4Pcode: 'C2528',
    admin1Name: 'Homs',
    admin1Pcode: 'SY04'
  }
];

export const homsAr = [
  {
    value: [36.57991039900003, 34.509591411000144],
    label: 'القصير',
    admin4Pcode: 'C2733',
    admin1Name: 'Homs',
    admin1Pcode: 'SY04'
  },
  {
    value: [36.731226002000085, 34.92409498800009],
    label: 'الرستن',
    admin4Pcode: 'C2870',
    admin1Name: 'Homs',
    admin1Pcode: 'SY04'
  },
  {
    value: [36.71973076900014, 34.72996072899997],
    label: 'حمص',
    admin4Pcode: 'C2528',
    admin1Name: 'Homs',
    admin1Pcode: 'SY04'
  }
];

export const aleppo = [
  {
    value: [36.86552261500009, 36.51109375000016],
    label: 'Afrin',
    admin4Pcode: 'C1366',
    admin1Name: 'Aleppo',
    admin1Pcode: 'SY02'
  },
  {
    value: [37.152419657999985, 36.20601312000002],
    label: 'Aleppo',
    admin4Pcode: 'C1007',
    admin1Name: 'Aleppo',
    admin1Pcode: 'SY02'
  },
  {
    value: [37.04315979100011, 36.58487207000013],
    label: 'Azaz',
    admin4Pcode: 'C1564',
    admin1Name: 'Aleppo',
    admin1Pcode: 'SY02'
  },
  {
    value: [37.95103912900002, 36.52578521600003],
    label: 'Menbij',
    admin4Pcode: 'C1767',
    admin1Name: 'Aleppo',
    admin1Pcode: 'SY02'
  }
];

export const aleppoAr = [
  {
    value: [36.86552261500009, 36.51109375000016],
    label: 'عفرين',
    admin4Pcode: 'C1366',
    admin1Name: 'Aleppo',
    admin1Pcode: 'SY02'
  },
  {
    value: [37.152419657999985, 36.20601312000002],
    label: 'حلب',
    admin4Pcode: 'C1007',
    admin1Name: 'Aleppo',
    admin1Pcode: 'SY02'
  },
  {
    value: [37.04315979100011, 36.58487207000013],
    label: 'أعزاز',
    admin4Pcode: 'C1564',
    admin1Name: 'Aleppo',
    admin1Pcode: 'SY02'
  },
  {
    value: [37.95103912900002, 36.52578521600003],
    label: 'منبج',
    admin4Pcode: 'C1767',
    admin1Name: 'Aleppo',
    admin1Pcode: 'SY02'
  }
];

export const asSweida = [
  {
    value: [36.567990777000034, 32.706810845000064],
    label: 'As-Sweida',
    admin4Pcode: 'C6147',
    admin1Name: 'As-Sweida',
    admin1Pcode: 'SY13'
  }
];

export const asSweidaAr = [
  {
    value: [36.567990777000034, 32.706810845000064],
    label: 'السويداء',
    admin4Pcode: 'C6147',
    admin1Name: 'As-Sweida',
    admin1Pcode: 'SY13'
  }
];

export const deirEzZor = [
  {
    value: [40.146070719000136, 35.33188505800018],
    label: 'Deir-Ez-Zor',
    admin4Pcode: 'C5086',
    admin1Name: 'Deir-ez-Zor',
    admin1Pcode: 'SY09'
  }
];

export const deirEzZorAr = [
  {
    value: [40.146070719000136, 35.33188505800018],
    label: 'دير الزور',
    admin4Pcode: 'C5086',
    admin1Name: 'Deir-ez-Zor',
    admin1Pcode: 'SY09'
  }
];

export const lattakia = [
  {
    value: [35.789569082000185, 35.53914895000014],
    label: 'Lattakia',
    admin4Pcode: 'C3480',
    admin1Name: 'Lattakia',
    admin1Pcode: 'SY06'
  }
];

export const lattakiaAr = [
  {
    value: [35.789569082000185, 35.53914895000014],
    label: 'اللاذقية',
    admin4Pcode: 'C3480',
    admin1Name: 'Lattakia',
    admin1Pcode: 'SY06'
  }
];

export const dara = [
  {
    value: [36.10494375400009, 32.624101815000074],
    label: "Dar'a",
    admin4Pcode: 'C5993',
    admin1Name: "Dar'a",
    admin1Pcode: 'SY12'
  },
  {
    value: [36.254165649, 32.86767326],
    label: 'Izra',
    admin4Pcode: 'C6101',
    admin1Name: "Dar'a",
    admin1Pcode: 'SY12'
  },
  {
    value: [36.04097121399997, 32.88824717900019],
    label: 'Nawa',
    admin4Pcode: 'C6124',
    admin1Name: "Dar'a",
    admin1Pcode: 'SY12'
  }
];

export const daraAr = [
  {
    value: [36.10494375400009, 32.624101815000074],
    label: 'درعا',
    admin4Pcode: 'C5993',
    admin1Name: "Dar'a",
    admin1Pcode: 'SY12'
  },
  {
    value: [36.254165649, 32.86767326],
    label: 'ازرع',
    admin4Pcode: 'C6101',
    admin1Name: "Dar'a",
    admin1Pcode: 'SY12'
  },
  {
    value: [36.04097121399997, 32.88824717900019],
    label: 'نوى',
    admin4Pcode: 'C6124',
    admin1Name: "Dar'a",
    admin1Pcode: 'SY12'
  }
];

export const alHasakeh = [
  {
    value: [40.74289887900011, 36.50537518200019],
    label: 'Al-Hasakeh',
    admin4Pcode: 'C4360',
    admin1Name: 'Al-Hasakeh',
    admin1Pcode: 'SY08'
  },
  {
    value: [41.22618305600008, 37.04474773200013],
    label: 'Quamishli',
    admin4Pcode: 'C4564',
    admin1Name: 'Al-Hasakeh',
    admin1Pcode: 'SY08'
  }
];

export const alHasakehAr = [
  {
    value: [40.74289887900011, 36.50537518200019],
    label: 'الحسكة',
    admin4Pcode: 'C4360',
    admin1Name: 'Al-Hasakeh',
    admin1Pcode: 'SY08'
  },
  {
    value: [41.22618305600008, 37.04474773200013],
    label: 'القامشلي',
    admin4Pcode: 'C4564',
    admin1Name: 'Al-Hasakeh',
    admin1Pcode: 'SY08'
  }
];

export const groupedOptions = [
  {
    label: 'Al-Hasakeh',
    options: alHasakeh
  },
  {
    label: 'Aleppo',
    options: aleppo
  },
  {
    label: 'Ar-Raqqa',
    options: arRaqqa
  },
  {
    label: 'As-Sweida',
    options: asSweida
  },
  {
    label: 'Damascus',
    options: damascus
  },
  {
    label: "Dar'a",
    options: dara
  },
  {
    label: 'Deir-Ez-Zor',
    options: deirEzZor
  },
  {
    label: 'Hama',
    options: hama
  },
  {
    label: 'Homs',
    options: homs
  },
  {
    label: 'Idleb',
    options: idleb
  },
  {
    label: 'Lattakia',
    options: lattakia
  },
  {
    label: 'Rural Damascus',
    options: ruralDamascus
  }
];

export const groupedOptionsAr = [
  {
    label: 'Al-Hasakeh',
    options: alHasakehAr
  },
  {
    label: 'Aleppo',
    options: aleppoAr
  },
  {
    label: 'Ar-Raqqa',
    options: arRaqqaAr
  },
  {
    label: 'As-Sweida',
    options: asSweidaAr
  },
  {
    label: 'Damascus',
    options: damascusAr
  },
  {
    label: "Dar'a",
    options: daraAr
  },
  {
    label: 'Deir-Ez-Zor',
    options: deirEzZorAr
  },
  {
    label: 'Hama',
    options: hamaAr
  },
  {
    label: 'Homs',
    options: homsAr
  },
  {
    label: 'Idleb',
    options: idlebAr
  },
  {
    label: 'Lattakia',
    options: lattakiaAr
  },
  {
    label: 'Rural Damascus',
    options: ruralDamascusAr
  }
];

export const options = [
  {
    value: [36.254165649, 32.86767326],
    label: 'Izra',
    admin4Pcode: 'C6101',
    admin1Name: "Dar'a",
    admin1Pcode: 'SY12'
  },
  {
    value: [37.051036244, 35.011318683],
    label: 'As-Salamiyeh',
    admin4Pcode: 'C3244',
    admin1Name: 'Hama',
    admin1Pcode: 'SY05'
  },
  {
    value: [36.311829455, 33.604157399],
    label: 'At Tall',
    admin4Pcode: 'C2395',
    admin1Name: 'Rural Damascus',
    admin1Pcode: 'SY03'
  },
  {
    value: [36.3149531, 35.814180826],
    label: 'Jisr-Ash-Shugur',
    admin4Pcode: 'C4199',
    admin1Name: 'Idleb',
    admin1Pcode: 'SY07'
  },
  {
    value: [36.635276032000036, 35.9290080450001],
    label: 'Idleb',
    admin4Pcode: 'C3871',
    admin1Name: 'Idleb',
    admin1Pcode: 'SY07'
  },
  {
    value: [39.00606189700011, 35.95307453000015],
    label: 'Ar-Raqqa',
    admin4Pcode: 'C5710',
    admin1Name: 'Ar-Raqqa',
    admin1Pcode: 'SY11'
  },
  {
    value: [36.3325752124, 33.523466849],
    label: 'Jawbar',
    admin4Pcode: 'C6001',
    admin1Name: 'Damascus',
    admin1Pcode: 'SY01'
  },
  {
    value: [36.3381133037, 33.543480566],
    label: 'Qaboun',
    admin4Pcode: 'C6002',
    admin1Name: 'Damascus',
    admin1Pcode: 'SY01'
  },
  {
    value: [36.76129068400007, 35.133741730000054],
    label: 'Hama',
    admin4Pcode: 'C2987',
    admin1Name: 'Hama',
    admin1Pcode: 'SY05'
  },
  {
    value: [36.40459234100007, 33.57057203900018],
    label: 'Duma',
    admin4Pcode: 'C2338',
    admin1Name: 'Rural Damascus',
    admin1Pcode: 'SY03'
  },
  {
    value: [36.57991039900003, 34.509591411000144],
    label: 'Al-Qusayr',
    admin4Pcode: 'C2733',
    admin1Name: 'Homs',
    admin1Pcode: 'SY04'
  },
  {
    value: [36.21734372400016, 33.54819815500008],
    label: 'Qudsiya',
    admin4Pcode: 'C2328',
    admin1Name: 'Rural Damascus',
    admin1Pcode: 'SY03'
  },
  {
    value: [37.152419657999985, 36.20601312000002],
    label: 'Aleppo',
    admin4Pcode: 'C1007',
    admin1Name: 'Aleppo',
    admin1Pcode: 'SY02'
  },
  {
    value: [36.86552261500009, 36.51109375000016],
    label: 'Afrin',
    admin4Pcode: 'C1366',
    admin1Name: 'Aleppo',
    admin1Pcode: 'SY02'
  },
  {
    value: [37.04315979100011, 36.58487207000013],
    label: 'Azaz',
    admin4Pcode: 'C1564',
    admin1Name: 'Aleppo',
    admin1Pcode: 'SY02'
  },
  {
    value: [36.731226002000085, 34.92409498800009],
    label: 'Ar-Rastan',
    admin4Pcode: 'C2870',
    admin1Name: 'Homs',
    admin1Pcode: 'SY04'
  },
  {
    value: [36.567990777000034, 32.706810845000064],
    label: 'As-Sweida',
    admin4Pcode: 'C6147',
    admin1Name: 'As-Sweida',
    admin1Pcode: 'SY13'
  },
  {
    value: [36.34831672200005, 33.48765732600014],
    label: 'Jaramana',
    admin4Pcode: 'C2304',
    admin1Name: 'Rural Damascus',
    admin1Pcode: 'SY03'
  },
  {
    value: [40.146070719000136, 35.33188505800018],
    label: 'Deir-Ez-Zor',
    admin4Pcode: 'C5086',
    admin1Name: 'Deir-ez-Zor',
    admin1Pcode: 'SY09'
  },
  {
    value: [35.789569082000185, 35.53914895000014],
    label: 'Lattakia',
    admin4Pcode: 'C3480',
    admin1Name: 'Lattakia',
    admin1Pcode: 'SY06'
  },
  {
    value: [36.10494375400009, 32.624101815000074],
    label: "Dar'a",
    admin4Pcode: 'C5993',
    admin1Name: "Dar'a",
    admin1Pcode: 'SY12'
  },
  {
    value: [41.22618305600008, 37.04474773200013],
    label: 'Quamishli',
    admin4Pcode: 'C4564',
    admin1Name: 'Al-Hasakeh',
    admin1Pcode: 'SY08'
  },
  {
    value: [36.04097121399997, 32.88824717900019],
    label: 'Nawa',
    admin4Pcode: 'C6124',
    admin1Name: "Dar'a",
    admin1Pcode: 'SY12'
  },
  {
    value: [40.74289887900011, 36.50537518200019],
    label: 'Al-Hasakeh',
    admin4Pcode: 'C4360',
    admin1Name: 'Al-Hasakeh',
    admin1Pcode: 'SY08'
  },
  {
    value: [36.71973076900014, 34.72996072899997],
    label: 'Homs',
    admin4Pcode: 'C2528',
    admin1Name: 'Homs',
    admin1Pcode: 'SY04'
  },
  {
    value: [37.95103912900002, 36.52578521600003],
    label: 'Menbij',
    admin4Pcode: 'C1767',
    admin1Name: 'Aleppo',
    admin1Pcode: 'SY02'
  }
];

export const govLabelCoordinates = [
  [36.98, 33.6], // 1
  [37.6, 35.8], // 2
  [37.65, 33.25], // 3
  [38.4741, 34.289], // 4
  [37.5293, 35.2187], // 5
  [35.35, 35.65], // 6
  [36.75, 35.5412], // 7
  [40.65, 36.2], // 8
  [40.5, 34.6], // 9
  [35.5, 34.9], // 10
  [38.8, 35.6], // 11
  [36.25, 33.1], // 12
  [36.9, 32.6], // 13
  [35.2, 33.0363] // 14
];

export const urbanUserGroups = ['wos', 'gos', 'nes', 'nws'];

/* GoS: Deir-Ez-Zor, Aleppo, Latakkia, Hama, Ar-Rastan, Homs, Al-Qusayr, Jawbar, Qaboun, Jaramana, Douma, Qudsiya, Dara, Nawa and As-Swedia, Izra, As-Salamiyeh, At Tall, Ar-Raqqa */
/* NES: Quamishli, Ar-Raqqa, Al-Hasakeh, Manbij */
/* NWS: Azaz, Afreen, Idleb, Jisr Ash Shugur, Ar-Raqqa */
export const urbanUserGroupsList = {
  nes: ['C4564', 'C5710', 'C4360', 'C1767'],
  nws: ['C1564', 'C1366', 'C3871', 'C4199', 'C5710'],
  gos: [
    'C6001',
    'C6002',
    'C2987',
    'C2338',
    'C2733',
    'C2328',
    'C1007',
    'C2870',
    'C6147',
    'C2304',
    'C5086',
    'C3480',
    'C5993',
    'C6124',
    'C2528',
    'C2395',
    'C3244',
    'C6101',
    'C5710'
  ]
};
