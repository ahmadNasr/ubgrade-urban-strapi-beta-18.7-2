'use strict';

/**
 * A set of functions called "actions" for `User`
 */

const crypto = require('crypto');
const _ = require('lodash');
const validateRegisterInput = require('../../../validation/register');
const validateLoginInput = require('../../../validation/login');
const validateForgotPasswordInput = require('../../../validation/forgot-password');
const validateResetPasswordInput = require('../../../validation/reset-password');
const validateContactInput = require('../../../validation/contact');

module.exports = {
  register: async ctx => {
    const settingsErrors = {};

    const pluginStore = await strapi.store({
      environment: '',
      type: 'plugin',
      name: 'users-permissions'
    });

    const settings = await pluginStore.get({
      key: 'advanced'
    });

    if (!settings.allow_register) {
      settingsErrors.allow_register = 'Register action is currently disabled.';
      return ctx.badRequest(null, settingsErrors);
    }
    const { errors, isValid } = validateRegisterInput(ctx.request.body);

    //Check validation
    if (!isValid) {
      return ctx.badRequest(null, errors);
    }

    const params = _.assign(ctx.request.body, {
      provider: 'local'
    });

    // Retrieve root role.
    const root = await strapi.plugins['users-permissions'].models.role
      .findOne({ type: 'root' })
      .populate('users');

    const users = root.users || [];
    // First, check if the user is the first one to register as admin.
    const hasAdmin = users.length > 0;

    // Check if the user is the first to register
    const role =
      hasAdmin === false
        ? root
        : await strapi.plugins['users-permissions'].models.role.findOne(
            { type: settings.default_role },
            []
          );

    if (!role) {
      return ctx.badRequest(
        null,
        ctx.request.admin
          ? [{ messages: [{ id: 'Auth.form.error.role.notFound' }] }]
          : 'Impossible to find the root role.'
      );
    }

    params.role = role._id || role.id;
    params.password = await strapi.api.user.services.user.hashPassword(params);

    const userDupName = await strapi.plugins[
      'users-permissions'
    ].models.user.findOne({
      username: params.username
    });

    if (userDupName && userDupName.provider === params.provider) {
      errors.username = 'Username is already taken';
      return ctx.badRequest(null, errors);
    }

    const user = await strapi.plugins['users-permissions'].models.user.findOne({
      email: params.email
    });

    if (user && user.provider === params.provider) {
      errors.email = 'Email is already taken';
      return ctx.badRequest(null, errors);
    }

    if (user && user.provider !== params.provider && settings.unique_email) {
      errors.email = 'Email is already taken';
      return ctx.badRequest(null, errors);
    }

    try {
      if (!settings.email_confirmation) {
        params.confirmed = true;
      }

      params.operations = '...';

      const user = await strapi.plugins['users-permissions'].models.user.create(
        params
      );

      const jwt = strapi.plugins['users-permissions'].services.jwt.issue(
        _.pick(user.toJSON ? user.toJSON() : user, ['_id', 'id'])
      );

      if (settings.email_confirmation) {
        const storeEmail =
          (await pluginStore.get({
            key: 'email'
          })) || {};

        const settings = storeEmail['email_confirmation']
          ? storeEmail['email_confirmation'].options
          : {};

        settings.message = await strapi.plugins[
          'users-permissions'
        ].services.userspermissions.template(settings.message, {
          URL: new URL(
            '/auth/email-confirmation',
            strapi.config.url
          ).toString(),
          USER: _.omit(user.toJSON ? user.toJSON() : user, [
            'password',
            'resetPasswordToken',
            'role',
            'provider'
          ]),
          CODE: jwt
        });

        settings.object = await strapi.plugins[
          'users-permissions'
        ].services.userspermissions.template(settings.object, {
          USER: _.omit(user.toJSON ? user.toJSON() : user, [
            'password',
            'resetPasswordToken',
            'role',
            'provider'
          ])
        });

        if (hasAdmin) {
          try {
            const rootRole = await strapi.plugins[
              'users-permissions'
            ].models.role
              .findOne({ type: 'root' })
              .populate('users');

            const adminEmail = rootRole.users.map(function(user) {
              return user.email;
            });

            const apiURL =
              process.env.NODE_ENV === 'production'
                ? strapi.config.environments.production.apiURL
                : strapi.config.environments.development.apiURL;

            const emailConfUrl = `${apiURL}/auth/email-confirmation`;

            const emailBody = `<h4>Hello UrbAN Admin,</h4>
  
          <h4>The following user wants to join the UrbAN web portal:</h4>
          
          <p>Name: ${user.name}</p>
  
          <p>Email: ${user.email}</p>

          <p>Area of Operations: ${params.operationsDesc}</p>
  
          <p>Position: ${user.position}</p>
  
          <p>Organization: ${user.organization}</p>
  
          <p>Country: ${user.country}</p>
  
          <p>Approve user with area of operations as:</p>
  
          <a href="${emailConfUrl}?confirmation=${jwt}&operations=wos" target="_blank">WoS</a>

          <br/>

          <a href="${emailConfUrl}?confirmation=${jwt}&operations=gos" target="_blank">GoS</a>

          <br/>

          <a href="${emailConfUrl}?confirmation=${jwt}&operations=nes" target="_blank">NES</a>
        
          <br/>

          <a href="${emailConfUrl}?confirmation=${jwt}&operations=nws" target="_blank">NWS</a>
        
          <br/>
  
          <h4>Have a great day! <br/> UrbAN Syria Team</h4>`;

            // Send an email to the admin.
            await strapi.plugins['email'].services.email.send({
              to: adminEmail,
              from:
                settings.from.email && settings.from.name
                  ? `"${settings.from.name}" <${settings.from.email}>`
                  : undefined,
              replyTo: settings.response_email,
              subject: settings.object,
              text: emailBody,
              html: emailBody
            });
          } catch (err) {
            return ctx.badRequest(null, err);
          }
        }
      }

      ctx.send({
        jwt,
        user: _.omit(user.toJSON ? user.toJSON() : user, [
          'password',
          'resetPasswordToken'
        ])
      });
    } catch (err) {
      const adminError = _.includes(err.message, 'username')
        ? 'Auth.form.error.username.taken'
        : 'Auth.form.error.email.taken';

      ctx.badRequest(
        null,
        ctx.request.admin ? [{ messages: [{ id: adminError }] }] : err.message
      );
    }
  },

  emailConfirmation: async ctx => {
    const params = ctx.query;

    if (params.operations && params.confirmation) {
      const userID = await strapi.plugins[
        'users-permissions'
      ].services.jwt.verify(params.confirmation);

      const user = await strapi.plugins[
        'users-permissions'
      ].models.user.findOne({
        _id: userID
      });

      if (user && user.confirmed === false) {
        await strapi.plugins['users-permissions'].models.user
          .findOne({
            _id: userID
          })
          .update({
            confirmed: true,
            operations: params.operations
          });

        try {
          const frontendURL =
            process.env.NODE_ENV === 'production'
              ? strapi.config.environments.production.frontendURL
              : strapi.config.environments.development.frontendURL;

          const emailBody = `<p>Hello,</p>
    
    <p>Thank you for registering!</p>

    <p>Your account has been approved, you can login and explore more products at:</p>

    <a href="${frontendURL}" target="_blank">UrbAN | SYRIA</a>
 
    <br/>

    <h4>Have a great day! <br/> UrbAN Syria Team</h4>`;

          // Send an email to the user.
          await strapi.plugins['email'].services.email.send({
            to: user.email,
            replyTo: user.email,
            subject: 'Your UrbAN | SYRIA Account is Approved!',
            text: emailBody,
            html: emailBody
          });
        } catch (err) {
          return ctx.badRequest(null, err);
        }
      }

      const settings = await strapi
        .store({
          environment: '',
          type: 'plugin',
          name: 'users-permissions',
          key: 'advanced'
        })
        .get();

      ctx.redirect(settings.email_confirmation_redirection || '/');
    } else {
      return ctx.badRequest(
        null,
        'Error! No token or Area of Operations is provided'
      );
    }
  },

  callback: async ctx => {
    const { errors, isValid } = validateLoginInput(ctx.request.body);

    //Check validation
    if (!isValid) {
      return ctx.badRequest(null, errors);
    }

    const provider = ctx.params.provider || 'local';
    const params = ctx.request.body;

    const store = await strapi.store({
      environment: '',
      type: 'plugin',
      name: 'users-permissions'
    });

    //return (ctx.body = [errors, params]);

    if (provider === 'local') {
      if (
        !_.get(await store.get({ key: 'grant' }), 'email.enabled') &&
        !ctx.request.admin
      ) {
        return ctx.badRequest(null, 'This provider is disabled.');
      }

      const user = await strapi.plugins['users-permissions'].models.user
        .findOne({
          email: params.identifier
        })
        .populate('role');

      if (!user) {
        errors.identifier = 'User not found';
        return ctx.badRequest(null, errors);
      }

      if (
        _.get(await store.get({ key: 'advanced' }), 'email_confirmation') &&
        user.confirmed !== true
      ) {
        errors.identifier = 'Sorry, your account is not confirmed yet!';
        return ctx.badRequest(null, errors);
      }

      if (user.blocked === true) {
        errors.identifier =
          'Sorry, your account has been blocked by the administrator';
        return ctx.badRequest(null, errors);
      }

      if (user.role.type !== 'root' && ctx.request.admin) {
        return ctx.badRequest(
          null,
          ctx.request.admin
            ? [{ messages: [{ id: 'Auth.form.error.noAdminAccess' }] }]
            : `You're not an administrator.`
        );
      }

      // The user never authenticated with the `local` provider.
      if (!user.password) {
        return ctx.badRequest(
          null,
          ctx.request.admin
            ? [{ messages: [{ id: 'Auth.form.error.password.local' }] }]
            : 'This user never set a local password, please login thanks to the provider used during account creation.'
        );
      }

      const validPassword = strapi.plugins[
        'users-permissions'
      ].services.user.validatePassword(params.password, user.password);

      if (!validPassword) {
        errors.password = 'Incorrect password';
        return ctx.badRequest(null, errors);
      } else {
        ctx.send({
          jwt: strapi.plugins['users-permissions'].services.jwt.issue(
            _.pick(user.toJSON ? user.toJSON() : user, ['_id', 'id'])
          ),
          user: _.omit(user.toJSON ? user.toJSON() : user, [
            'password',
            'resetPasswordToken'
          ])
        });
      }
    } else {
      if (!_.get(await store.get({ key: 'grant' }), [provider, 'enabled'])) {
        return ctx.badRequest(null, 'This provider is disabled.');
      }

      // Connect the user thanks to the third-party provider.
      let user, error;
      try {
        [user, error] = await strapi.plugins[
          'users-permissions'
        ].services.providers.connect(provider, ctx.query);
      } catch ([user, error]) {
        return ctx.badRequest(
          null,
          error === 'array' ? (ctx.request.admin ? error[0] : error[1]) : error
        );
      }

      if (!user) {
        return ctx.badRequest(
          null,
          error === 'array' ? (ctx.request.admin ? error[0] : error[1]) : error
        );
      }

      ctx.send({
        jwt: strapi.plugins['users-permissions'].services.jwt.issue(
          _.pick(user, ['_id', 'id'])
        ),
        user: _.omit(user.toJSON ? user.toJSON() : user, [
          'password',
          'resetPasswordToken'
        ])
      });
    }
  },
  forgotPassword: async ctx => {
    const { errors, isValid } = validateForgotPasswordInput(ctx.request.body);

    //Check validation
    if (!isValid) {
      return ctx.badRequest(null, errors);
    }

    const frontendURL =
      process.env.NODE_ENV === 'production'
        ? strapi.config.environments.production.frontendURL
        : strapi.config.environments.development.frontendURL;

    const { email } = ctx.request.body;
    const url = `${frontendURL}/user/reset-password`;

    const user = await strapi.plugins['users-permissions'].models.user.findOne({
      email: email
    });

    // User not found.
    if (!user) {
      errors.email = 'User not found';
      return ctx.badRequest(null, errors);
    }

    // Generate random token.
    const resetPasswordToken = crypto.randomBytes(64).toString('hex');

    // Set the property code.
    user.resetPasswordToken = resetPasswordToken;

    const settings = (await strapi
      .store({
        environment: '',
        type: 'plugin',
        name: 'users-permissions'
      })
      .get({ key: 'email' }))['reset_password'].options;

    settings.message = await strapi.plugins[
      'users-permissions'
    ].services.userspermissions.template(settings.message, {
      URL: url,
      USER: _.omit(user.toJSON ? user.toJSON() : user, [
        'password',
        'resetPasswordToken',
        'role',
        'provider'
      ]),
      TOKEN: resetPasswordToken
    });

    settings.object = await strapi.plugins[
      'users-permissions'
    ].services.userspermissions.template(settings.object, {
      USER: _.omit(user.toJSON ? user.toJSON() : user, [
        'password',
        'resetPasswordToken',
        'role',
        'provider'
      ])
    });

    try {
      // Send an email to the user.
      await strapi.plugins['email'].services.email.send({
        to: user.email,
        from:
          settings.from.email || settings.from.name
            ? `"${settings.from.name}" <${settings.from.email}>`
            : undefined,
        replyTo: settings.response_email,
        subject: settings.object,
        text: settings.message,
        html: settings.message
      });
    } catch (err) {
      return ctx.badRequest(null, err);
    }

    await strapi.plugins['users-permissions'].models.user
      .findOne({
        email: email
      })
      .update({
        resetPasswordToken: resetPasswordToken
      });

    ctx.send(true);
  },

  resetPassword: async ctx => {
    const { errors, isValid } = validateResetPasswordInput(ctx.request.body);

    //Check validation
    if (!isValid) {
      return ctx.badRequest(null, errors);
    }

    const params = ctx.request.body;
    params.code = ctx.params.code;

    if (params.code) {
      const user = await strapi.plugins[
        'users-permissions'
      ].models.user.findOne({
        resetPasswordToken: params.code
      });

      if (!user) {
        errors.code = 'Invalid reset code!';
        return ctx.badRequest(null, errors);
      }

      // Delete the current code
      user.resetPasswordToken = null;

      user.password = await strapi.plugins[
        'users-permissions'
      ].services.user.hashPassword(params);

      await strapi.plugins['users-permissions'].models.user
        .findOne({
          email: user.email
        })
        .update({
          resetPasswordToken: user.resetPasswordToken,
          password: user.password
        });

      ctx.send(true);
    } else {
      errors.code =
        'Something went wrong! Please use the reset password link sent on your email...';
      return ctx.badRequest(null, errors);
    }
  },

  contact: async ctx => {
    const { errors, isValid } = validateContactInput(ctx.request.body);

    //Check validation
    if (!isValid) {
      return ctx.badRequest(null, errors);
    }

    const params = ctx.request.body;

    const pluginStore = await strapi.store({
      environment: '',
      type: 'plugin',
      name: 'users-permissions'
    });

    const settings = await pluginStore.get({
      key: 'advanced'
    });

    // Retrieve root role.
    const root = await strapi.plugins['users-permissions'].models.role
      .findOne({ type: 'root' })
      .populate('users');

    const users = root.users || [];
    // First, check if the user is the first one to register as admin.
    const hasAdmin = users.length > 0;

    // Check if the user is the first to register
    const role =
      hasAdmin === false
        ? root
        : await strapi.plugins['users-permissions'].models.role.findOne(
            { type: settings.default_role },
            []
          );

    if (!role) {
      return ctx.badRequest(
        null,
        ctx.request.admin
          ? [{ messages: [{ id: 'Auth.form.error.role.notFound' }] }]
          : 'Impossible to find the root role.'
      );
    }

    if (hasAdmin) {
      try {
        const adminEmail = root.users.map(function(user) {
          return user.email;
        });

        const emailBody = `<h4>Hello UrbAN Admin,</h4>

        <h4>You have received the following contact request:</h4>
        <p>Sender Name: ${params.name}</p>

        <p>Sender Email: ${params.email}</p>

        <p>Message:</p>
        
        <p>${params.message}</p>
        <br/>
        <h4>Have a great day! <br/> UrbAN Syria Team</h4>`;

        await strapi.plugins['email'].services.email.send({
          to: adminEmail,
          replyTo: `"${params.name}" <${params.email}>`,
          subject: `Contact Request from ${params.email}`,
          text: emailBody,
          html: emailBody
        });

        /* await strapi.plugins['email'].services.email.send({
          to: params.email,
          subject: `urban-syria.org contact request`,
          text: emailBody,
          html: emailBody
        }); */

        return ctx.send(true);
      } catch (err) {
        return ctx.badRequest(null, err);
      }
    }
  }
};
