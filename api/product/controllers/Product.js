'use strict';
// Source: http://koajs.com/#stream

/**
 * Product.js controller
 *
 * @description: A set of functions called "actions" for managing `Product`.
 */

const fs = require('fs');
const path = require('path');
const isEmpty = require('../../../validation/is-empty');
const { PDFDocument, StandardFonts } = require('pdf-lib');

module.exports = {
  recentCityProfileReport: async ctx => {
    return await Product.aggregate([
      {
        $match: {
          type: 'City Profile Report',
          city: { $ne: null },
          document: { $eq: null }
        }
      },
      {
        $lookup: {
          from: 'city',
          localField: 'city',
          foreignField: '_id',
          as: 'city'
        }
      },
      {
        $lookup: {
          from: 'upload_file',
          localField: '_id',
          foreignField: 'related.ref',
          as: 'document'
        }
      },
      { $sort: { createdAt: 1 } },
      {
        $group: {
          _id: '$city._id',
          doc: { $first: '$$ROOT' }
        }
      },
      {
        $replaceRoot: { newRoot: '$doc' }
      },
      {
        $unwind: '$city'
      }
    ])
      .then(city => {
        if (!city) {
          return false;
        }
        return city;
      })
      .catch(err => {
        return false;
      });
  },
  recentCityProfileReportSkeleton: async ctx => {
    return await Product.aggregate([
      {
        $match: {
          type: 'City Profile Report',
          city: { $ne: null },
          document: { $eq: null }
        }
      },
      {
        $lookup: {
          from: 'city',
          localField: 'city',
          foreignField: '_id',
          as: 'city'
        }
      },
      { $sort: { createdAt: 1 } },
      {
        $group: {
          _id: '$city._id',
          doc: { $first: '$$ROOT' }
        }
      },
      {
        $replaceRoot: { newRoot: '$doc' }
      },
      {
        $unwind: '$city'
      }
    ])
      .then(city => {
        if (!city) {
          return false;
        }
        return city;
      })
      .catch(err => {
        return false;
      });
  },
  recentCityFactSheet: async ctx => {
    return await Product.aggregate([
      {
        $match: {
          type: 'City Fact Sheet',
          city: { $ne: null },
          language: { $eq: 'en' },
          document: { $eq: null }
        }
      },
      {
        $lookup: {
          from: 'city',
          localField: 'city',
          foreignField: '_id',
          as: 'city'
        }
      },
      {
        $lookup: {
          from: 'upload_file',
          localField: '_id',
          foreignField: 'related.ref',
          as: 'document'
        }
      },
      { $sort: { createdAt: 1 } },
      {
        $group: {
          _id: '$city._id',
          doc: { $first: '$$ROOT' }
        }
      },
      {
        $replaceRoot: { newRoot: '$doc' }
      },
      {
        $unwind: '$city'
      }
    ])
      .then(city => {
        if (!city) {
          return false;
        }
        return city;
      })
      .catch(err => {
        return false;
      });
  },
  recentCityFactSheetAr: async ctx => {
    return await Product.aggregate([
      {
        $match: {
          type: 'City Fact Sheet',
          city: { $ne: null },
          language: { $eq: 'ar' },
          document: { $eq: null }
        }
      },
      {
        $lookup: {
          from: 'city',
          localField: 'city',
          foreignField: '_id',
          as: 'city'
        }
      },
      {
        $lookup: {
          from: 'upload_file',
          localField: '_id',
          foreignField: 'related.ref',
          as: 'document'
        }
      },
      { $sort: { createdAt: 1 } },
      {
        $group: {
          _id: '$city._id',
          doc: { $first: '$$ROOT' }
        }
      },
      {
        $replaceRoot: { newRoot: '$doc' }
      },
      {
        $unwind: '$city'
      }
    ])
      .then(city => {
        if (!city) {
          return false;
        }
        return city;
      })
      .catch(err => {
        return false;
      });
  },
  downloadPrivateCopiedProduct: async ctx => {
    const product = ctx.request.body;
    if (!isEmpty(product)) {
      if (ctx.state.user && ctx.state.user.email) {
        const privateFilesPath =
          process.env.NODE_ENV === 'production'
            ? strapi.config.environments.production.privateFilesPath
            : strapi.config.environments.development.privateFilesPath;

        const copiedFilesPath =
          process.env.NODE_ENV === 'production'
            ? strapi.config.environments.production.copiedFilesPath
            : strapi.config.environments.development.copiedFilesPath;

        const fileName = product.name;
        const fileExt = fileName.substring(fileName.lastIndexOf('.') + 1);
        const tempFileName = `${ctx.state.user
          .email}_${product.city}_${product.type}_${+new Date()}.${fileExt}`;

        const filePath = path.join(
          __dirname,
          '..',
          '..',
          '..',
          copiedFilesPath,
          `${tempFileName}`
        );

        if (product.type === 'City Profile Report') {
          const file = fs.readFileSync(`.${privateFilesPath}/${product.name}`);

          const pdfDoc = await PDFDocument.load(file);
          const helveticaFont = await pdfDoc.embedFont(StandardFonts.Helvetica);
          const { email } = ctx.state.user || '';
          pdfDoc.getPages().forEach(page => {
            page.drawText(`UrbAN-S Portal - ${email}`, {
              x: 42,
              y: 20,
              size: 8,
              font: helveticaFont
            });
          });
          const pdfBytes = await pdfDoc.save();

          fs.writeFile(filePath, pdfBytes, err => {
            if (err) console.log(`Error while writing file to server!`);
            //console.log('The file has been saved!');
          });
        }
        else {
          fs.copyFileSync(
            `.${privateFilesPath}/${product.name}`,
            `.${copiedFilesPath}/${tempFileName}`
          );
        }

        ctx.body = filePath;
      }
      else {
        console.log('You need to log in to download this product!');
        return ctx.badRequest(
          'Error, you need to log in to download this product!'
        );
      }
    }
    else {
      console.log('Error, product options are not sent!');
      return ctx.badRequest('Error, product options are not sent!');
    }
  },
  deletePrivateCopiedProduct: async ctx => {
    const filePath = ctx.request.body;
    if (filePath) {
      setTimeout(function() {
        fs.unlink(filePath, err => {
          if (err) console.log(`${filePath} was not deleted`);
          console.log(`${filePath} was successfully deleted`);
        });
      }, 600000);
      return true;
    }
    else {
      console.log('Error, product file path is not provided!');
      return ctx.badRequest('Error, product file path is not provided!');
    }
  }
};
