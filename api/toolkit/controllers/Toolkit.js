'use strict';

/**
 * Toolkit.js controller
 *
 * @description: A set of functions called "actions" for managing `Toolkit`.
 */

module.exports = {
  getToolkitProduct: async ctx => {
    return await Toolkit.findOne({
      type: ctx.params.type
    })
      .populate('document')
      .select('document')
      .then(tool => {
        if (!tool || !tool.document || !tool.document.url) {
          return false;
        }
        return tool.document.url;
      })
      .catch(err => {
        return err;
      });
  }
};
